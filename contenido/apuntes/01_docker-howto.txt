## OPCION 1	(Repo Ubuntu Bionic Beaver)
##==========
sudo apt install docker.io	# Instalar docker

sudo systemctl start docker	# Arrancar el servicio
sudo systemctl enable docker	# Arrancar en el boot
docker --version		# Comprobar la versión

## OPCION 2	(Desde el sitio Docker)
##==========
# Programas necesarios
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common

# Conseguir la clave
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt update

# Install Docker CE
sudo apt install docker-ce
docker --version

## Uso: docker [opcion] [comando] [argumentos]
=======

# Se descarga y ejecuta la imagen del contenedor hello-world.
# Dockerhub
$ docker run hello-world

# Buscar todos los contenedores que contengan la palabra Ubuntu
$ docker search ubuntu

# Descargar una imagen
$ docker pull ubuntu

# Ver los contenedores instalados
$ docker images

# Ejecutar un contenedor
$ docker run -it ubuntu
# Equivale a: systemd-nspawn -bD <maquina>

# Se pueden instalar cosas dentro de la máquina. Ejemplo:
$ apt update
$ apt install nodejs
$ node -v

#Para no tener que poner sudo:
sudo usermod -aG docker ${USER}
su - ${USER} # relogearse
id -nG       # Comprobar que estoy en el grupo docker

#Ayuda:
docker <comando> --help
docker info # Información de cómo está el sistema.

# Para ver los procesos que corre docker:
docker ps
docker ps -a # a: all
docker ps -l # l: last

# Gestionar un contenedor:
docker start <id | nombre>
docker stop <id | nombre>
docker rm <id | nombre>

# Se pueden guardar las imágenes creadas en docker hub
# usando docker commit

