vim: vi mejorado

Se crea para que no tengamos que levantar las manos. Ni hacia el ratón, ni hacia las flechas.

Tiene dos modos:
1. Modo Comando: Esc
1. Modo inserción.
1. Modo visual
1. Modo Bloque visual


Modo Inserción: 
 - i (insert), I (Insert at the beginning)
 - a (append), A (Append at the end)
 - o (insert belOw), O (insert befOre)

<nr> Modo Comando

## Ejecución Inmediata
### Comandos
Movimientos
j: 	Baja
k:	Sube
h:	Izquierda
l:	Derecha
C-u:    Re pag
C-d:	Av pag
C-f:    Av Pan
C-b:    Retr Pan

C-a: Incrementar
C-x: Decrementar

w:	Va al inicio de la siguiente palabra (W)
e: 	Va a la última letra de la palabra. (E)
b:	Retrocede una palabra. (B)

{:
}:	Avanzar de párrafo en párrafo.
(:
): 	Avanzar de sentencia en sentencia.

0: Ir al principio de la linea
$: Ir al final de la linea
7G: Ir a la linea 7

gg: Ir al principio del fichero
G: Ir al final del fichero.

%: Encontrar al compañero.

u: undo
Ct-Sh-r: Redo

p: Pegar después. Paste
P: Pegar antes.

J:   Unir lineas. Join
x: corta una letra
r: reemplaza una letra

## Operadores. Comandos con ámbito
d: Delete
c: Change

dd: Borra toda la linea.
y: Yank. Copiar

### Ámbitos
w: palabra
s: Frase. Sentencia.
p: párrafo
b: Paréntesis.(. block
B: Llaves. {

i: interno. ib (Texto entre paréntesis). inner
a: Incluyendo lo anterior.


## Linea de comandos
:set nu => Poner el número de linea
:set nonu => Quitar las lineas

f<letra>: Busca las siguiente <letra>

:w (write file)
:w! Sobreescribir
:wq Guardar y salir
:e editar
:e! editar nuevo sin guardar actual
:q Salir
:q! Salir sin guardar
:split  Dividir la pantalla
:vsplit Dividirla en vertical.
!: Terminal

/<palabra>: Buscar <palabra>
n: Buscar siguiente ocurrencia. next
N: Buscar anterior ocurrencia.
?<palabra>: Buscar hacia atrás.
*: Buscar la palabra bajo el cursor.
 #: Buscar la palabra bajo el cursor, pero hacia atrás.

:set hlsearch

Expresiones regulares
Caracteres especiales en las busquedas:
.	Cualquier caracter
*	0 o más
[]
^
%
/
\
?
~
$

## Rangos
% Todas las lineas del documento
$ Última linea
. Linea actual
Expresion regular
Marcas manuales

## Partciones y pestañas
Particiones / paneles
:split
:vsplit
C-w [hjkl]: Moverse entre pestañas
C-w c: Cerrar el panel
C-w o: Cerrar todos los paneles menos el activo
C-w +: Aumenta el panel
C-w -: Disminuye el panel
C-w =: Iguala la anchura de los paneles.
C-w _: Maximiza la altura del panel activo
C-w |: Maximiza la anchura
[N] C-w _: Altura de N filas
[N] C-w |: Anchura de N columnas

Pestañas
:tabnew
:tabe[dit] [archivo]
:tabc[lose]
:tabo[nly]
:tabn[ext]
:tabp[revious]


