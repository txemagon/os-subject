
The truth: ====>/home/txema/apuntes.txt <==== (the one and only)

Ruta absoluta:   Nombre del archivo
+------------+   +-----------+
|/home/txema/|   |apuntes.txt|
+------------+   +-----------+

basename:   extension:
+-------+   +----+
|apuntes|   |.txt|
+-------+   +----+

Otra ruta absoluta:
+---------------------------------+
|/home/txema/Documentos/os-subject|    0j0 => Lo entiendo.
+---------------------------------+

Otra ruta absoluta (que no existe):
+----------------------------+
|/txema/Documentos/os-subject|         0j0 => No exite.
+----------------------------+


No lo entiendo. Ruta relativa (a donde estoy ahora):
+---------------------------+
|txema/Documentos/os-subject|         0j0 => No lo entiendo.
+---------------------------+			|
                                                V
                            Añadir `pwd` <=  Denken Sie, Bite.
                                 |
                    No existe <--+---> Existe

mv Beispiele:
=============

/home/txema/Descargas/bio.txt => /home/txema/Documentos/bio.txt

mv /home/txema/Descargas/bio.txt /home/txema/Documentos/bio.txt

[@/home]
mv txema/Descargas/bio.txt txema/Documentos/bio.txt

[@/home/txema]
mv Descargas/bio.txt Documentos/bio.txt

[@/home/txema/Descargas]
mv bio.txt ../Documentos/bio.txt

mv bio.txt biografia.txt

mv bio.txt ../Documentos/biografia.txt

cp Beispiele:
=============
cp /home/txema/Descargas/bio.txt /home/txema/Documentos/bio.txt
cp bio.txt biografia.txt

Damos a entender que se llama como antes
cp /home/txema/Descargas/bio.txt /home/txema/Documentos/

cp /home/txema/Descargas/bio.txt /home/txema/Documentos


