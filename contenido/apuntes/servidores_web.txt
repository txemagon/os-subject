# URI
  protocolo://[usuario:[contraseña]@]dominio[:puerto]/ruta

# Búsqueda DNS
  Archivo Hosts -> servidor DNS

# Subir una página

servidor:/var/www/html$ nc -l 8888 | gunzip | sudo tar xvf -
cliente:~/work$ tar cvf - aprobados/ | gzip | nc -q0 localhost 8888 

Asegurarse de que el nombre de usuario es correcto y hay apache tiene permiso para
acceder al contenido.
servidor$ chown -R www-data:www-data aprobados

## Mejora

Subir todas las versiones y hacer un enlace simbólico a la última que funcione.
servidor$ sudo ln -s /var/www/html/aprobados-v2 aprobados

# Configurar un host virtual
1. Crear el sitio en /etc/apache2/sites-available
2. Habilitarlo con: sudo a2ensite
3. Recargar la configuración de Apache. systemctl reload apache2
4. Crear acceso al servicio
	a. En local mediante el archivo /etc/hosts
        b. Globalmente configurando un servidor BIND9 con los registros A.
5. Comprobar con un navegador que accedemos al sitio.


#Logs
servidor$ tail -f /var/log/apache2/error.log
servidor$ tail -f /var/log/apache2/access.log

