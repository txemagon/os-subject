# Temas y Vídeos

1. Instalación del Sistema Operativo.
    1. Ubuntu
    1. Arch Linux
    1. Hackintosh Ryzentosh
2. Jerarquía del sistema de ficheros.
3. Las terminales.
4. Los editores.
5. Configuración de red.
6. Gestión de paquetes.
7. Altas y bajas de usuarios.
8. Procesos y servicios.
9. El sistema de entrada / salida.
10. Filtros.
11. Jaulas.
12. Contenedores.

