# PREGUNTAS


1. Crear un directorio.

```bash
user@host:/var/www$ # Crear el directorio /home/smyr01/tmp/pepe
user@host:/var/www$ mkdir -p ~/tmp/pepe
```

1. Cambiar el nombre al directorio

```bash
user@host:/var/www$ # Haz que 'pepe' se llame 'juan'.
user@host:/var/www$ mv ~/tmp/pepe ~/tmp/juan
```

1. Mover el directorio.

```bash
user@host:/var/www$ # Haz que 'juan' esté en 'tmp/pepe.
user@host:/var/www$ mkdir ~/tmp/pepe
user@host:/var/www$ mv ~/tmp/juan ~/tmp/pepe/juan
```

1. Crear un fichero.
```bash
user@host:/var/www$ # Pon 'Hola' en el fichero '~/tmp/pepe/juan/saluda.txt'
user@host:/var/www$ echo Hola > ~/tmp/pepe/juan/saluda.txt
```

1. Crear un fichero vacío.
```bash
user@host:/var/www$ # Crea el fichero 'hola.md' en el directorio '~/tmp/pepe/juan/dice/'
user@host:/var/www$ mkdir ~/tmp/pepe/juan/dice/
user@host:/var/www$ touch ~/tmp/pepe/juan/dice/hola.md
```

1. Ir a un directorio con idea de volver.
```bash
user@host:/var/www$ # Vete al directorio '~/tmp/pepe/juan/dice/'.
user@host:/var/www$ pushd ~/tmp/pepe/juan/dice/
```

1. Crear varios ficheros.
```bash
user@host:~/tmp/pepe/juan/dice$ # Crea 'hole.md', 'holi.md' y 'holu.md' con un sólo comando.
user@host:~/tmp/pepe/juan/dice$ touch hol{e,i,u}.md
```

1. Muestra una secuencia.
```bash
user@host:~/tmp/pepe/juan/dice$ # Muestra los números de '0' a '9'.
user@host:~/tmp/pepe/juan/dice$ seq 0 9
```

1. Crear varios ficheros.
```bash
user@host:~/tmp/pepe/juan/dice$ # Crea los ficheros 'hola_0X.md', donde X son cada uno de los valores entre '0' y '9'.
user@host:~/tmp/pepe/juan/dice$ for i in `seq 0 9`; do touch hola_0$i.md ; done
```

1. Listar los ficheros
```bash
user@host:~/tmp/pepe/juan/dice$ # Escribe 'Fichero: XXXXX' mostrando todos los ficheros que sean 'hola_0X.md' donde X es uno de estos números: 0 1 2 3 4 5 7
user@host:~/tmp/pepe/juan/dice$ for i in `echo hola_0[0-57].md`; do echo "Fichero: $i": done
```

1. Listar los ficheros
```bash
user@host:~/tmp/pepe/juan/dice$ # Lista con el mismo formato todos los ficheros 'hola_0X.md' que no hayas listado en el caso anterior.
user@host:~/tmp/pepe/juan/dice$ for i in `echo hola_0[^0-57].md`; do echo "Fichero: $i"; done
```

1. Listar los ficheros
```bash
user@host:~/tmp/pepe/juan/dice$ # Muestra todos los ficheros que están en el directorio '$HOME'
user@host:~/tmp/pepe/juan/dice$ ls -a ~
```

1. Copiar un directorio
```bash
user@host:~/tmp/pepe/juan/dice$ # Subir al directorio 'juan'
user@host:~/tmp/pepe/juan/dice$ cd ..
user@host:~/tmp/pepe/juan$ # Copiar el directorio 'dice' en el directorio 'pepe'
user@host:~/tmp/pepe/juan$ cp -R dice ../
```

1. Copiar un directorio

```bash
user@host:~/tmp/pepe/juan$ # Copiar el contenido de 'dice' sin copiar el directorio 'dice' dentro del directorio 'pepe'.
user@host:~/tmp/pepe/juan$ cp -R dice/* ../
```

1. Renombrar ficheros
```bash
user@host:~/tmp/pepe/juan$ # Cambiar todos los 'hola_XX.md' por 'hello_xx.md' 
user@host:~/tmp/pepe/juan$ rename 's/hola/hello/' dice/hola_??.md
```

1. Enlace duro (hard link)
```bash
user@host:~/tmp/pepe/juan$ # Crear un acceso al fichero 'dice/hello_00.md' con el nombre 'pantagruel.md'
user@host:~/tmp/pepe/juan$ ln dice/hello_00.md pantagruel.md
```

1. Enlace simbólico (soft link)
```bash
user@host:~/tmp/pepe/juan$ # Crear un acceso al fichero 'dice/hello_00.md' con el nombre 'gargantua.md'
user@host:~/tmp/pepe/juan$ ln -s dice/hello_00.md gargantua.md
```

1.- Hacer un script
```bash
user@host:~/tmp/pepe/juan$ # En el directorio 'scripts' hacer el script 'listar' que mustre el contenido del directorio en formato largo y ejecutarlo.
user@host:~/tmp/pepe/juan$ mkdir scripts
user@host:~/tmp/pepe/juan$ cd scripts
user@host:~/tmp/pepe/juan/scripts$ echo '#!'`which bash` > listar
user@host:~/tmp/pepe/juan/scripts$ chmod +x listar
user@host:~/tmp/pepe/juan/scripts$ echo ls -lh >> listar
user@host:~/tmp/pepe/juan/scripts$ 
```

1. Script con parámetros

```bash
user@host:~/tmp/pepe/juan/scripts$ # Hacer un script 'saluda0 que nos salude por nuestro nombre y diga el número de parámetros.
user@host:~/tmp/pepe/juan/scripts$ cat << FIN > saluda
!/usr/bin/bash
# Este programa saluda

echo "Número de parámetros: $#"
echo
echo "Parámetros: $*"
echo
echo "Hola, $1"
echo
echo "Programa: $0"
FIN
user@host:~/tmp/pepe/juan/scripts$ chmod +x saluda
user@host:~/tmp/pepe/juan/scripts$ ./saluda Jorge
```


1. Retornar a un directorio.
```bash
user@host:~/tmp/pepe/juan/dice$ popd
```
