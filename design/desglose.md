# PRÁCTICAS SO



## BLOQUE 1: Instalación en VirtualBox

### Contenidos Previos

1. Hablar de los distintos tipos de virtualización (barebone (XEN) - VirtualBox, VMWare)
1. Hablar de los distintos sistemas operativos y su distribución en el mercado.
  1. Escritorio: W2us, Linux, Mac (UNIX BSD + Cocoa)
  1. Empotrados (Móviles, IoT)
1. Configuración del equipo para instalar VirtualBox
  1. Activar la virtualización por hardware en la BIOS
  1. Configurar la maquina Virtual en VirtualBox

### Prácticas

#### Práctica 1: Planificación de la instalación.

1. Requisitos de Memoria, Disco Duro, Tarjeta Gráfica.
1. Particiones de los discos duros: sistema, swap, datos, otros sistemas.

#### Práctica 2: Instalación

1. Instalación del SO.
1. Determinación de los programas a instalar
1. Configuración del arranque.
1. Conky


#### Práctica 3 (en casa)

1. Instalación de otro OS, grabando un vídeo con el enlace a Youtube


#### Práctica 4 (en casa)

1. Instalación de un mac (raizentosh?)


### BLOQUE 2


### BLOQUE 3

## BLOQUE 3: Cloud

  1. LVM, Kubernetes, USBs de arranques (dd), XEN, Kubernetes, MinIO, ¿Docker?

