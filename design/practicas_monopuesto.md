# INSTALACIÓN SISTEMA

- Instalación de Disco Desatendido
- VirtualBox
- VMWare (player - ¿licencia?)
- Xen
- Kubernetes
- MinIO
- Actualización del sistema (do-release-upgrade)

## Base

1. Jails (chroot)
1. Systemctl
1. Docker

# MANEJO BÁSICO DEL SISTEMA

1. Sistemas de Ficheros
    1. Permisos
1. Comandos
1. Vim

# GESTIÓN DE USUARIOS

- Ficheros importantes
- getent
- Altas y modificacions
- ldap


# PERSONALIZACIÓN DEL SISTEMA

1. Software Base
1. Escritorios
1. Widgets

# FALLOS

- Rotura de Disco
- Recuperación de Contraseñas
- Manejo de Logs


# CONFIGURACIÓN

- Red
- Panel de Control
  - Agregar y Quitar Programas
- Registro
- Gestión de Servicios
- Gestión de tareas
- apt
- gpg keys
- pacman
- compilación


# INTERCOMUNICACION

1. Impresoras
1. Recursos compartidos
1. Escritorio Remoto
1. Samba
1. FTP
1. SSH
1. nc

# MANTENIMIENTO

1. Backups rotativos
1. Supervisión de Ataques
1. Últimas conexiones