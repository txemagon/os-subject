# Apuntes de Instalación de Sistemas Operativos

## ARCH

De la [guía de instalación](https://wiki.archlinux.org/title/Installation_guide), 

```bash
ls /usr/share/kbd/keymaps/**/*.map.gz  # Lista las distribuciones de teclado
loadkeys es				# Cargar la distribución de teclado española.
ip link				# Comprobar las tarjetas de red disponibles.
ping archlinux.org			# Comprobar la conexión hacia el exterior.
timedatectl status			# Comprobar que la hora es correcta
fdisk -l				# Listar las particiones de disco

fdisk /dev/sda				# Crear partición
					# n: Nueva
					# p: primaria
					# 1: Número
					# Primer sector
					# Último sector
					# w: Escribir cambios
mkfs.ext4 /dev/sda1			# Crear el sistema de ficheros
mount /dev/sda1 /mnt			# Montar el disco
pacstrap /mnt base			# Instalar los paquetes básicos
pacstrap -K /mnt base linux linux-firmware	# Instalar los paquetes básicos
genfstab -U /mnt >>/mnt/etc/fstab	# Generar las opciones de montaje
arch-chroot /mnt			# Cambiar el raíz
ln -sf /usr/share/zoneinfo/Europe/Madrid /etc/localtime	# Establecer la zona horaria
hwclock --systohc			# Generar /etc/adjtime
pacman -S nano				# Instalar un editor
# Hablilitar el idioma local
/usr/bin/nano /etc/locale.gen			# Descomentar la línea es_ES.UTF-8 UTF-8
locale-gen				# Generar los archivos locale
echo 'LANG=es_ES.UTF-8' >/etc/locale.conf # Poner la variable LANG
echo LAB70PC01 > /etc/hostname		# Poner nombre al equipo
# mkinitcpio -P			# Crear ramfs
passwd					# Cambiar la contraseña de root

# Instalar el gestor de arranque
pacman -S grub
grub-install --target=i386-pc /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

# Reiniciar
exit
umount -R /mnt
reboot
```


Otro tutorial: https://gist.github.com/thomasheller/5b9b18917bbaabceb4f629b793428ee2
