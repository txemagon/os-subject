# Instalación del Sistema

## Conseguir el sistema

```bash
wget -> Conseguir el OS
```

| Partición | Punto de Montaje | Tamaño | Tipo del SF | Comando              |
|-----------|------------------|--------|-------------|----------------------|
| Boot:     |		        | 500Mb  | t: EFI      | fdisk                | 
| Primaria  | Raíz: /	        | 500Gb  | EXT4        | mkfs.ext4            |
| home      |       /home	| Resto  | EXT4 / NTFS | mkfs.ext4, mkfs.ntfs | 
| swap:     |			| 8Gb    | swap        | mkswap, swapon       |


Mostrar las particiones:

```bash
lsblk  	# Dispositivos de almacenamiento masivo
fdisk -l  	# Mostrar las particiones

fdisk /dev/sda  # Crear las particiones
```



