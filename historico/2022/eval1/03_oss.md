# ESTRUCTURA DEL SISTEMA OPERATIVO

##

Su nombre: GNU  

o   

GNU/linux  

(GNU) es un acrónimo recursivo.

((GNU) is Not Unix)
(((GNU) is Not Unix) is Not Unix)
((((GNU) is Not Unix) is Not Unix) is Not Unix))

Suena como la palabra new (nuevo) y significa que no es
UNIX repetido hasta el infinito.  

## Partes


![](img/gnu.png)

### Accesos

![Niveles de acceso](img/os_layers.png)

**BASH**: Bourne Again SHell (Terminal). *Richard Stallman*.  
**linux**: Kernel (núcleo). Hardware. *Linus Torvalds*.  

## JERARQUÍA DEL SISTEMA DE FICHEROS

_File Hierarchy System_

![Mount Diagram](img/mount-diag.jpg)

### Según el Instalador

| Instalador    | Tipo              |   Directorio     |
|---------------|-------------------|------------------|
|    GNU        | Sistema Operativo | `/` (Raíz)       |
|    Debian     | Distribución      | `/usr`	        |
| Administrador | Multiusuario      | `/usr/local`     |
| Usuario       | Monousuario       | `~/bin`          |

### Directorios Notables

Que almacenan ficheros.

| Directorio | Propósito |
|------------|-----------|
| bin   | Ejecutables para los usuarios. |
| sbin  | Ejecutables para el administrador del sistema. |
| etc   | Configuración general[^1] de programas y servicios. |
| lib   | Librerías compiladas del sistema |
| share | Documentación y librerías no compiladas del sistema. |
| share/doc | Documentación. |
| opt   | Paquetes instalados opcionalmente. |
| var   | Ficheros que varían (game scores, logs, páginas web). |
| var/tmp | Ficheros temporales que el sistema guarda durante 30 días. |
| tmp   | Ficheros temporales hasta el siguiente reinicio o 10 días. | srv   | Documentos que distribuiran los servidores. |
| boot  | Configuración del arranque. |
| home  | Directorio de todos los directorios HOME de cada usuario. |
| root  | HOME del administrador del sistema. |

Que almacenan pseudoficheros que mapean hardware.

| Directorio | Propósito |
|------------|-----------|
| dev	| Donde se montan los dispositivos (devices). |
| dev/fd | Descriptores de los ficheros actualmente abiertos. |
| dev/snd | Hardware de sonido. |
| dev/tcp | Conexiones activas de red. |
| media | Montaje de dispositivos de almacenamiento. |
| mnt   | Directorio de montaje por defecto. |
| sys	| Mapeo del hardware: buses, sistemas de ficheros, etc. |
| proc  | Mapeo de los procesos activos. |
| proc/self | Proceso que se está ejecutando. |
| run   | Sistema de ficheros temporal para montar durante el arranque. |
 

[^1]: Algunos programas admiten configuraciones _por usuario_.


### Ficheros Notables


`/dev/null`: No contiene nada se eche lo que se eche. Vale para silenciar comandos.

```bash
echo hola &> /dev/null
```

`/dev/zero`:  Siempre leemos un cero desde él.  
`/dev/random`:  Genera números aleatorios. Necesita la participación del usuario para poder generarlos.  

`/dev/urandom`:  Genera números pseudoaleatorios. No necesita la participación del usuario para poder generarlos.  

`/dev/pts/`: Se almacenan las pseudoterminales.  

`/var/log/auth.log`: Accesos al sistema.  
`/var/log/syslog`: Diagnóstico del sistema.  

