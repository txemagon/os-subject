# SCRIPTING

## Notas preliminares

Empleo del $:

`$VAR`: Dime lo que vale VAR  
`${VAR}`: Dime lo que vale VAR  
`$(seq 1 20)`: Ejecuta seq y pon aquí el resultado. Equivalente al backtick.  
`$(( 2 + 3))`: Expansión aritmética. Opera y pon aquí el resultado.  

Process Substitution:

Ejecutar un comando y acceder a su salida como si fuera un fichero.

1. `<(list)`  
1. `>(list)`  

## Consideraciones Generales

1. Es vital comentar
1. `#!` se llama shebang e indica con qué interprete de comandos se ejecutará el código.

## Variables y Parámetros

### De las Variables

Una variable es una caja en la que puedo guardar un dato y a la que le puedo poner una etiqueta.

Por ejemplo, puedo guardar el valor juanma en la caja que tiene la etiqueta USER.

```bash
USER=Juanma
```

De la obligatoriedad de las llaves:

```bash
#!/usr/bin/bash
# ProgramaTxote v1.0

USER=Juanma

echo "Txematxote"
echo $USERtxote    # No existe la variable USERtxote.
echo $USER txote   # Espacio en blanco no deseado.
echo ${USER}txote  # Son imprescindibles las llaves.
```

### De los Parámetros:

Los parámetros son variables numeradas que contienen el valor inicial correspondiente a los argumentos.

Si tenemos el siguiente programa:

```bash
#!/usr/bin/bash
# programaTxote v1.0
# 
# ./programaTxote Juanma


echo ${1}txote  # Son imprescindibles las llaves.
```

y lo ejecutamos de diversas maneras podemos observar las siguientes salidas:

```bash
$ ./programaTxote Juanma
JuanmaTxote

$ ./programaTxote te
teTxote
```

SHELL VARIABLES

La terminal ya viene con distintas variables predefinidas. Para saber más sobre ellas consulta el documento de [variables de entorno](./02.2_entorno.md)




## Parámetros

`$0`: Nombre del script.  
`$*`: Cadena con todos los argumentos.  
`$@`: Lista completa de argumentos.   
`$#`: Número total de parámetros.  
`shift`: Desplaza los parámetros.  


ALIAS
ARRAYS
HASH
VARIABLES DE ENTORNO
= :=

