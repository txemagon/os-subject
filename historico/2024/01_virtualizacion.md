# Virtualización

## Elementos Previos

### BIOS

BIOS: Basic Input Output System
                                  
```                                  
     ┌─────┐                      
     │ RAM ┼───────────┐          
     └──▲──┘           │          
        │              │          
        │           ┌──▼───┐      
        │           │  uP  │      
        │           └───▲──┘      
        │               │         
    ┌───┼──┐            │  Boot   
    │  HD  │            │         
    └──┬───┘        ┌───┼──┐      
       └────────────► BIOS │      
                    └──────┘      
```
                              
                                  
Virtualización por Hardware

### Discos Duros

1. Los datos están fragmentados. Y se pueden defragmentar.
1. Hay abundancia de ficheros. Los agrupamos por nombre.


```
            Nombre Completo del Fichero
                          |    
                          V
redaccion.doc => ofimatica/redaccion.doc
    ^                 
    |
  Nombre del fichero

```

Partes del nombre:

```
      Directorio -----+
                      |    
                      V
redaccion.doc => ofimatica/redaccion.doc
    ^      ^           
    |      +-- Extensión
  Nombre 

```

Directorio
Fichero
Archivo: Fichero que contiene ficheros.

```
  SISTEMA DE ARCHIVO  
                      
       DIRECTORIO     
                      
      Nombres iNodo   
                      
      res.doc   4     
                      
      art.txt   4     
      -------------   
                      
        DatoP1D01     
                      
        DatoP1D02     
                      
        DatoP1D03     
                      
        DatoP4D01 |   
                  |   
        DatoP4D02 |   
                      
        DatoP3D01     
       ──             
        DatoP3D02     
                      
        DatoP3D03     
                      
        DatoP4D03 |   
                  |   
        DatoP4D04 |   
                      
                      
```

Sistema de Archivos

- Windows: NTFS, FAT
- GNU/Linux : EXT4
- Mac : HFS

### RED

NAT

```
                                                     10.X.X.X                                                      
                                                                                                                   
                                                     172.16.X.X                                                    
                                                                                                                   
         85.48.72.13        │      ─┐                192.168.X.X                                                   
                            │       │                                                                              
                            │       │                                                                              
         IP PUBLICA         │       │           IP                                                                 
                            │       │          ┌─────────────────┐                                                 
         ┌──────────────────┼───┐   │NAT       │                 │                 VirtualBox                      
         │                      │   │          │                 │                                                 
         │                      │   │          │                 │                                                 
         │                      │   │          │                 │             ┌───────────────────────┐           
         └──────────────────────┘   │          │                 │             │                       │           
             MAC       IP PRIVADA   │          │                 │   NAT       │                       │           
                                    │          │                ─┼─────────────┼──                     │           
                       192.168.0.1             │                 │             │                       │           
                            ▲                  │                 │             │                       │           
                            │                  │                 │             │                       │           
                            │                  │                 │   A. PUENTE │                       │           
                            │                  │                ─┼─────────────┼──                     │           
                            │                  │                 │             │                       │           
                            │                  │                 │             │                       │           
                            └──────────────────┼                 │             │                       │           
                                               │                 │             └───────────────────────┘           
                                               │                 │                                                 
                                               │                 │                                                 
                                               │                 │                                                 
                                               │                 │                                                 
                                               └─────────────────┘                                                 
                                                                                                                   
                                                 MAC                                                               
                                                                                                                   
                                                                                                                   
                                                                                                                   
                                                                                                                   
                                                                                                                   
                                                                                                                   
```

Adaptador Puente

### Gráficos

GDI: Graphical Device Interface

- Sistema original de gráficos en Windows.
- Es especialista en pintar cajas 2D


DirectX

- HAL. Hardware Abstraction Layer. Acceso directo a la tarjeta.
- HEL. Hardware Emulation Layer. Aunque no te lo creas estoy usando GDI por debajo.

### Emulación de Unidades

ISO: Copia byte a byte de un CD.
