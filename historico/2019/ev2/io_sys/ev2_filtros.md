# Filtros

## Arquitectura

### Toolchain.
Concatenar unos comandos con otros a través de las tuberías.
Se les llama filtros porque la información cambia al atravesarlos.


### Modelo Bazar
Para que funcione bien el sistema hace falta que cada aplicación
haga una cosa y sólo una.

Hay como muchas pequeñas aplicaciones en vez de una grande: como en un 
bazar.

## Comandos

### Filtros que unen

`cat`: Puede funcionar como filtro si el nombre del fichero es: `-`

    # Copiar un fichero a otra máquina
    cat nombre.txt | ssh smyr01@172.19.70.9 "cat - > ~/nombre.txt"

`paste`: Lo opuesto de cut.  

```bash
    paste -d= alumnos.txt ev2.txt
```

`join`: Combina dos ficheros usando un campo de clave.  
```bash
join -j1 2 -o 1.1 2.2 poblaciones codigos
# -j1 2: Del fichero 1, usar la columna 2 como clave
# -o 1.1. 2.2: Output. Del fichero 1 mostrar col 1 y del f2, col 2.
```
### Filtros que cambian

`tr`:  Traduce caracteres individuales por otros.

Ejemplos:

    cat romeo.txt | tr i !              # Cambia las i por !
    cat romeo.txt | tr abce-z ABCE-Z    # A mayus todo menos la d
    cat romeo.txt | tr aeiou AE         # a->A ;; Resto->E
    cat romeo.txt | tr -c a-zA-Z =      # Conjunto complementario
    cat romeo.txt | tr -s " " " "       # Squeeze. Varios seguidos por uno.
    cat romeo.txt | tr -d i             # Borrar todas las i's.


### Filtros que cortan

#### ... en Horizontal
`tail`: Muestra el final de un archivo.  

    tail -50 romeo.txt  # Muestra las últimas 50 líneas.
    tail -f /var/sys/log/auth.log   # No cierra el archivo.

`head`: Muestra la cabecera de un fichero.  


`split`: Separa un fichero en lineas.  
`split [opciones] fichero prefijo`  

    # Divide cada 1000 lineas romeo.txt
    cat romeo.txt | split - partido/romeo.
    split romeo.txt partido/romeo.
    # Cada 10kb
    split -b10K romeo.txt partido/romeo.
    

#### ...en Vertical

`cut`: Separa por columnas.  

    # Coger emails de los contactos de google.
    # -f: field => Número de campo.
    # -d: delimiter => Delimitador. Por defecto: TAB
    cat contacts.csv | cut -d, -f28 | tr -s "\n" "\n"

Para ver varios campos:

```bash
    cat notas.txt | cut -f1,3  # Campos 1 y 3
    cat notas.txt | cut -f1-3  # Campos 1, 2 y 3.
    # Poner todas las líneas menos la primera
    cat notas.txt | cut -f1,3 | tail -$(( $(cat notas.txt | wc | tr -s " " " " | cut -d" " -f2) - 1))
```
### Filtros que cuentan

`wc`: word count. Cuenta lineas, palabras y caracteres.  

### Otros

`tee`: Hacer un empalme en T de la salida:

    ------|tee+|----- stdout
              |
             Fichero

    cat romeo.txt | tr i ! | tee copia_romeo.txt | tr -sc a-zA-Z "\n"

`sort`: Ordena alfabéticamente.  

    -n: numéricamente
    -u: único.

```bash
# Hacer una lista con las 3000 palabras 
# menos frecuentes de romeo y julieta
cat romeo.txt | tr -cs "a-zA-Z" "\n" | tr A-Z a-z | \
    sort | uniq -c | sort -n | head -3000 | \
    tr -s " " " " | cut -d " " -f3 > vocab3000.txt
```

`uniq`: Elimina las lineas repetidas juntas.  

    -c:  count. Cuenta las repeticiones



```bash
# Ordenar según el valor de la segunda columna.
paste <(cat notas2.txt | cut -f2) notas2.txt | tail -6 | sort -n | cut -f2-
```
### Que comparan

`comm f1 f2`: Compara dos ficheros ordenados.
`cmp`: Comparan si dos ficheros son iguales.  
`diff`: Muestra las diferencias entre dos ficheros.  

```
    <: Primer fichero
    >: Segundo fichero.
    c: Cambio
    a: El segundo fichero añade algo.
    d: El segundo fichero borra (delete) algo.
```

```bash
  diff -e f1 f2 # Genera los comandos del editor para que se pueda
                # pasar de un fichero a otro
```

### Visten:

`nl`: Numera líneas que no están vacías.  
`pr`: Prepara para imprimir.  

### Buscan

`grep`: Expresiones regulares antiguas.  

```bash
grep -v miercoles dias.txt      # Todas las líneas en las que
                                # no aparece miercoles.
cat romeo.txt | grep -n kill    # Numera las líneas.
# Expresiones regulares
cat romeo.txt | grep -n '[0-9]' # Busca un numero.
# * significa 0 o más
cat raras.txt | grep -n 'aqui[0-9]*[ae]no'
grep -n '^He' romeo.txt         # ^: Inicio de línea.
grep -n 'es$' dias.txt          # $: Final de línea
```

`egrep`: Expresiones regulares modernas.  

### Ejemplos

```bash
# Extraer ordenado por nombre los platos
# de cada comensal usando la dieta como clave
join dieta menu | tee j.txt | cut -d" " -f2 > nombres.txt && cat j.txt | cut -d" " -f1,3 | sort | paste nombres.txt - && rm j.txt
```
