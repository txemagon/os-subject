# Compresión

## TAR

Tape ARchive

Opciones:

* `c`: create
* `v`: verbose
* `f <fichero>`: file

```bash
# Archivar el directorio tmp.
tar cvf resumen.tar tmp/
# Archivar y comprimir
tar cv tmp/ | zip > clase.tar.gz
tar cvzf clase.tgz tmp/

#Extraer archivo
tar xvf clase.tar

#Extraer archivo comprimido
tar xvzf clase.tar.gz
```

## zip unzip

```bash
zip myfile.zip directorio/*   # Ficheros en tmp
zip -r myfile.zip directorio  # Ficheros en tmp y subdirs
unzip myfile.zip
```

## pigz

```bash
# Enviar
tar cf - tmp/ | pigz | nc -q0 localhost 8888
# Recibir
nc -l 8888 | pigz -d | tar xf - -C prueba/
```










