# Flujos de Información

## ASCII

American Santard Code for Information Interchange  

|Posición | Contenido | Ejemplos  |
|---------|-----------|-----------|
| 0x00 - 0x29| Códigos de control | 0x0A:NL 0x0D:CR |
| 0x30 - 0x39| Números    | 0 - 9  |
| 0x41       | Mayúsculas | 'A' - 'Z' |
| 0x61       | Minúsculas | 'a' - 'z' |

###ANSI

Códigos de control que cambian colores, posiciones de pantalla, borran
la pantalla y redefinen teclas.

    `echo -e "\x1B[34mHola\x1B[00m" # Imprime Hola en azul`


## Streams

En un sistema se puede mover la información a nivel de Hardware (capa 2).

O _pensando que hay tubos_: una abrastacción (capa 3).

23 en modo texto
y 23 3n modo binario.

### Standard Streams

0:stdin  
1:stdout  
2:stderr  

## Redirecciones de Salida

![](../img/streams_redir.png)

`>`: Borrar y crear.  
    `2>`: Redirige stderr.  
`>>`: Anexar (append).  

`2>&1`: Redirección de copia. Mete 2 donde va 1.  
    echo hola >/dev/null 2>&1
    echo hola 2>/dev/null >&2
`echo "hola" &> /dev/null`: Lleva el 2 y el 1 a `/dev/null`.  

`exec 3<> fichero`: Abre el stream (tubo) 3 en modo lectura y escritura hacia un fichero.  

      echo 1234567890 > fichero
      exec 3<> fichero
      read -n 4 <&3
      echo -n . >&3
      exec 3>&-
      cat fichero

`3>&-`: Cierra el tubo 3.  

## Redirecciones de Entrada

`<`:  Redirección de Entrada.  
`<<`: HERE DOCUMENT.  
`<<<`: HERE STRING.  

## Redirección entre Comandos

![](../img/streams_pipe.png)

`|`: Redirige a y desde un fichero anónimo.


### Ejemplos
Programa que cierra temporalmente stdout:

    #!/bin/bash
    # Este programa cierra temporalmente stdout.
    
    exec 3>&1   # Creo un tubo para "guardar" dónde va el 1.
    exec 1>&-   # Cierro el 1.
    
    echo "hola" # Debería no salir en pantalla.
    
    exec 1>&3   # Llevar el 1 a donde va el 3
    exec 3>&-   # Cerrar el 3
    
    echo "Ahora sí"


