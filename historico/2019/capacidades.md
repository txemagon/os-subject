# Capacidades

Las capacidades de las evaluaciones anteriores siguen siendo necesarias en las
evaluaciones venideras.

## Evaluación 1

Lo que debo ser capaz de hacer.  

### Instalación del SO

Puntos de interés:

1. Elegir un SO para satisfacer unas necesidades concretas.
1. Instalar un SO en VBox.

### Recolectar información del Hardware

Puntos de interés:

1. Saber cuantos núcleos tiene un equipo (`lscpu`).
1. Saber la velocidad máxima del procesador
1. Saber el nombre del microprocesador.
1. Saber cuantos discos duros hay (`lshw`). 
1. Ver los discos, las particiones y lo que ocupan (`lsblk`).

### Gestión del Sistema de Ficheros

Puntos de interés:

1. Punto de montaje.
1. Copiar (`cp`), renombrar (`mv`) y eliminar(`rm`) un fichero.
1. Eliminar un directorio vacío (`rmdir`)
1. Eliminar un directorio lleno con todos sus subdirectorios (`rm -rf`).
1. Crear un directorio (`mkdir`) creando también los padres si hace falta.
1. Saber cambiar los permisos de un fichero (`chmod`).
1. Saber cambiar el propietario de un fichero (`chown`).
1. Asignar varios nombres a un inodo (`ln`).
1. Crear un enlace simbólico (`ln -s`).
1. Saber crear un fichero vacío (`touch`).
1. Saber ver en qué directorio estoy (`pwd`).


### Concreción
Tareas concretas:

1. Procesar un conjunto de imágenes con ImageMagick.
1. Generar diagramas con GraphViz.
1. Escribir un documento con markdown.
1. Crear Sistemas de Ficheros.
    1. Copia de Seguridad en un Fichero
    1. Hacer un CD a mano.


## Evaluación 2

Puntos de interés:

1. Inspeccionar ficheros en modo binario.
    1. Uso de octal dump para ficheros de datos.
    1. Inspección de ficheros objeto: `nm`, `objdump`, `ldd`.
1. Inspeccionar ficheros en modo texto: `cat`, `tail`, `head`.
1. Manejo de las redirecciones de entrada, salida, copia y cierre.
1. Manejo de filtros.
1. Comprobar si dos ficheros de configuración son idénticos. Encontrar la primera diferencia.
1. Ser capaz de enviar y recibir ficheros entre equipos con scp y nc

### Concreción

Tareas Concretas:

1. Inspeccionar un fichero binario.
1. Partir un fichero en trozos ( `split` )y volver a montarlo ( `cat` ).
1. Controlar los ficheros de log del sistema en tiempo real.
1. Copiar un fichero usando `cat`.
1. Alimentar un programa con las redirecciones.
1. Eliminar la salida de errores redireccionando a `/dev/null`.
1. Hacer un editor de textos con `cat` y las redirecciones de entrada y salida.
1. Reproducir un archivo desde la terminal.
    1. Saber convertir de mp3 a wav.
1. Conseguir una página web desde /dev/tcp
1. Copiar un archivo de un equipo a otro usando `cat` y `ssh` exclusivamente.
1. Ser capaz de mostrar todas las notas de un fichero de notas de cada alumno, seleccionando automáticamente la cantidad de lineas a mostrar.
1. Unión con join
    * Sacar las cualidades de todos los personajes Harry Potter
    * Sacar todas capitales de un continente.
    * Sacar los géneros que trabaja un actor.
    * Sacar el período que estuvo activa una empresa de videojuegos.

