# Listado de Tareas para Casa

## Miércoles 11/03 y Jueves 12/03

### Objetivos
Como necesitamos tener una cuenta de gitlab. Las tareas de hoy son:

- Crear dicha cuenta y el repositorio, tal como fue mencionado en el
documento trabajo_casa.md
- Aprender a manejar git.

### Contenido
Para ello os dejo unos enlaces.

- La guía sencilla. Esto, a pesar de su nombre, es un chuletario. Un sitio al que volver cuando no te acuerdas de algo. https://rogerdudler.github.io/git-guide/index.es.html
- Explicación más detallada que hay que leer. https://www.diegocmartin.com/tutorial-git/
- Además de las dos líneas anteriores hay montones de videos en internet que puedes ver, pero que no te eximen de leer los dos enlaces anteriores.

### Preguntas de autoevaluación
Además de eso tienes que contestar las siguientes preguntas. Pero, al margen de que tengas que
subir las respuestas aségurate de que sabes contestarlas. El conocimiento es para tí, y luego
lo tendrás que demostrar en un exámen. Prefiero que las contestes muy escuetamente por tus
medios a que hagas corta-pega.

1. ¿ Qué es un repositorio ?
1. ¿ Qué es una rama ?
1. ¿ Son origin y local dos repositorios ?
1. ¿ Qué es .gitignore ?
1. Busca 5 ejemplos de .gitignore en repositorios de gitlab.
1. Describe los siguientes comandos.
    * status
    * add
    * commit
    * clone
    * push
    * pull
    * checkout
    * branch
    * log
    * merge
1. Explica `git add -u`

Para los que estáis teniendo problemas he realizado un pequeño screencast: https://youtu.be/egK49jXYPOM

## Viernes 13

Hoy vamos a aprender a dar de alta usuarios en Linux.

Aquí tienes un tutorial sencillo y completo que explica el tema. Léelo detalladamente antes de seguir.

https://www.atareao.es/como/gestion-de-usuarios-y-grupos-en-linux/

Responde a las siguientes preguntas:
 
 Grupos
 1. Explica cada uno de los campos de ésta línea del fichero /etc/group. sambashare:x:129:lorenzo,pepe,juan
 1. Lista sólo los grupos de tú equipo.
 1. Añade el grupo juegos y ocio a tu equipo usando una sóla instrucción.
 1. Borra el usuario ocio

Usuarios
1. Explica cada uno de los campos de ésta línea del fichero /etc/passwd. lorenzo:x:1000:1000:Lorenzo,,,:/home/lorenzo:/bin/bash
 Sube los ejercios a gitlab. Gitlab sella cuándo se han subido las cosas. No lo dejes para otro día.
1. Muestra los usuarios normales.
1. Explica `grep -v`
1. Añade el usuario manolito.
1. Bórralo.
1. Explica el comando `groups`
1. Explica las opciones de `sudo usermod -a -G <grupos> <usuario>`
1. Realiza un ejemplo del comando anterior.
1. Muestra en tú equipo que ha funcionado.
1. Borra a un usuario de un grupo mediante `deluser`.
1. Explica el comando `finger`

## Miércoles 18

Trabajo para el delegado: Necesito, por favor que me compartas una hoja de cálculo de google drive en la que aparezca el
nombre de los chicos de clase, su email, y su url de gitlab, Y, los demás, enviádle, por favor la información de la manera
y forma que a él le sea más conveniente.

Hoy, vamos a empezar viendo una utilidad: ```xclip``` que comunica el portapapeles nuestro sistema de ventanas con el de la terminal.
El portapapeles, es un término que ha caído en desuso, así que puede que no lo conozcáis, pero es el lugar donde se almacena lo que
copiamos con Ctl C para luego pegarlo. Os dejo la URL con la información: https://www.atareao.es/como/del-terminal-al-portapapeles-con-xclip/

Y unos ejercicios, que, acordáos debéis documentar con el historial, con alguna captura de pantalla o con un screencast[^1] y que gitlab registra vuestra actividad diaria. No lo dejéis:

[^1]: Como utilizáis linux podeis grabar la pantalla y el micrófono con dos programas gratuitos: kazam y obs. El segundo es más profesional.
También se puede editar el vídeo con kdenlive u openshot. Lo valoraré positivamente ya que en la época de mis padres era analfabeto el que no sabía
leer y escribir, en la mía el que no sabe inglés. Pero, en la vuestra no va a serlo el que no sepa chino, sino el que no sepa editar vídeo. ¡Ánimo! A por ello.

1. Usando cat y una tubería a xclip copia el contenido de un archivo para luego pegarlo en un gedit.
1. Usando exclusivamente xclip copia el contenido de un fichero para pegarlo en un gedit.
1. Copia con `Ctl c` el contenido de un gedit y pégalo en un fichero desde la terminal.
1. Haz un alias para copiar y pegar. Usa los alias que has creado para repetir los ejercicios anteriores.



## Jueves 19

Un proceso es un programa en ejecución. Cuando abrimos dos ventanas de firefox tenemos un solo programa, pero dos procesos.
Cuando estamos en la terminal y ejecutamos un programa desde ella el sistema operativo hace lo siguiente:

1. El proceso terminal se duplica y aparece un proceso terminal hijo que depende de la terminal original.
1. El proceso terminal hijo hereda las tuberías de lectura y escritura del proceso padre.
1. La terminal padre sustituye el programa que se está ejecutando en la terminal hija (la propia terminal) por
el programa que queríamos arrancar.

Durante la ejecución:

1. Se envía una señal de error si el padre y el hijo acceden a las tuberías simultáneamente. Es por esto que cuando arrancamos un programa
desde la terminal, la terminal se queda parada.
1. Si el programa padre se cierra, el sistema operativo envía la señal SIGHUP (hang up - colgar) indicando que debe terminar la ejecución del hijo.

Es posible, además lanzar un programa para que se ejecute en segundo plano. Esto lo podemos conseguir si ponemos un & cómo último carácter de la línea. Prueba a poner: `gedit &`. Es muy interesante asegurarse que dicho programa no va a usar las tuberías de entrada/salida del padre.

Para comprender este tema vamos a seguir hoy los siguientes tutoriales.

1. https://www.atareao.es/como/procesos-en-paralelo-en-linux/ (hoy jueves)
1. https://www.atareao.es/como/procesos-en-segundo-plano-en-linux/ (hoy jueves)
1. https://www.atareao.es/como/disown-desacoplar-procesos/    (mañana viernes)
1. https://maslinux.es/que-es-el-comando-nohup-y-como-usarlo/  (mañana viernes)

Copia todos los scripts e instrucciones de los tutoriales y muéstralos bien en el historial, bien subiendo los ficheros script que hayas hecho.
Te recuerdo que se valorará que todos los días hagas un screencast con tu sesión de práctica. Puedes subirlos a tu canal de youtube y poner un enlace aquí en gitlab.

Responde a las siguientes preguntas de autoevaluación:
1. ¿Qué hace el comando `date`? Mira `man`. Explica `date +%s%3N`
1. ¿Qué hace `tarea.sh`?
1. Explica usando los tiempos de `tarea` el tiempo que te da `time` en el caso de `secuencial`.
1. Explica usando los tiempos de `tarea` el tiempo que te da `time` en el caso de `paralelo`.

Mira el manual para `xargs` y busca ayuda en Internet.

1. Explica la siguiente línea.
```bash
time echo 4 Tarea1 3 Tarea2 2 Tarea3 1 Tarea4 | xargs -n 2 -P 4 ./tarea.sh
```

Mira la ayuda para el comando `parallel`.

1. Muestra con un ejemplo (pantallazo) cómo funciona. Incrusta el pantallazo en el archivo de soluciones usando los enlaces de markdown.
1. Explica la opción -j
1. Explica y muestra un ejemplo de `--link`.

Notas:

1. Ejecutar algo entre `$()` quiere decir ejecuta un comando y pon aquí su salida.
1. Cuando ejecutamos: `./tarea.sh 1 tarea1`, dentro del script todas las apariciones de `$1` se sustituirán por `1` y `$2` por `tarea1`. En conclusióón, `$1` y `$2` son los parámetros de la línea de comandos.
1. En un script todo lo que empieza por `#` es un comentario.
1. Cuando la primera línea de un script tiene un shebang `#!` indica con qué programas hay que interpretar el contenido del fichero.




## Viernes 20

Cuando se abre un proceso y a éste se le asigna un sistema de tuberías se dice que se ha creado un **grupo de procesos** y
que este primer proceso es el **proceso líder**. El conjunto se llama: **sesión**. Por eso cuando abrimos una conexión `ssh` se dice que hemos
abierto una sesión (conseguido un set de tuberías) en el equipo. Sin embargo, cuando nos autentificamos con un usuario en el sistema de ventanas,
el que inició sesión fue la ventana que nos muestra los usuarios. Esta ventana se llama _display manager_ y no _session manager_ (gestor de sesiones).

Como administradores de sistema a veces nos es muy útil iniciar sesión en un equipo remoto y arrancar un programa. Si recuerdas de ayer, al cerrar la ventana padre, se le enviará una señal SIGHUP que cerrará todo lo que hayamos arrancado. Y, esto va a ocurrir aunque lo hayamos arrancado en segundo plano haciendo uso del mal llamado ampersand (o mejor llamado amperset).

Para ello, hoy vamos a ver la siguiente documentación: https://www.atareao.es/como/disown-desacoplar-procesos/


Tienes que leer la documentación y vas a crear el fichero tiempo:

```bash
#!/bin/bash
# tiempo

watch -n1 date +%H:%M:%S
```
Acuérdate de hacer `chmod +x tiempo` para poder ejecutarlo (`./tiempo`). Este script, `./tiempo`, sustituye donde pone `python3 -u test.py`

Preguntas de autoevaluación:

1. Explica qué hace la secuencia de teclas Ctrl-Z
1. Explica qué hace `jobs`.
1. Explica qué hace `bg`.
1. Explica qué hace `fg`.
1. Graba un screencast (puedes usar obs) mostrando los comandos anteriores.
1. Explica qué hace `ps`.
1. Explica `ps aux`.
1. Explica `ps -ef`.
1. Explica qué hace `disown` y sus opciones r y a.
1. Explica el comando `kill`.
1. Explica el comando `pkill`.
1. Explica el comando `nohup`.

Con todo lo aprendido haz el siguiente trabajo. Haz un screencast para documentarlo.

1. Abre una conexión `ssh` a tu ordenador: `ssh localhost`
1. En esa sesión ejecuta el comando `./tiempo &`
1. Sal de la sesión escribiendo `exit`.
1. Muestra desde la terminal con `ps aux | grep tiempo` que el programa sigue corriendo.


## Miércoles 25

Hoy hemos abierto el chat y algunos de vosotros me habéis dicho que vais un poco retrasados. Así que vamos a hacer una parada y con el chat nos vamos poniendo al día.


## Miércoles 13 Mayo

1. Instalación de, al menos, tres máquinas virtuales. Una de ellas ha de ser Manjaro. Contará extra la instalación de Arch Linux y la de Hackintosh. Hay que subir a gitlab al menos 5 capturas de pantalla de cada máquina. Una de las capturas tiene que mostrar el nombre del usuario en la instalación y otra tiene que mostrar la termianl funcionando.

