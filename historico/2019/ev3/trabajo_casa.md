# Trabajo desde Casa

## Una Palabra Previa

Quería recordaros, chicos, algunos puntos. Primeramente, aunque los colegios estén cerrados nosotros
vamos a intentar seguir con la mayor normalidad posible, pero desde luego el esfuerzo que hacemos los
profesores para que estéis atentos en clase, no lo vamos a poder seguir ejerciendo. Y, esto necesita
de una madurez de vuestra parte: que vuestro "barco" no se hunda depende de que os empeñéis y os
comprometáis desde hoy mismo con vuestro trabajo diario. Y como a la pereza la conocemos todos, os
diría que respetar los horarios de clase y sentaros a hacer los deberes a vuestra hora va a ser
uno de vuestros mayores aliados.

En segundo lugar, recordáos que no estamos de vacaciones. Es posible que otros colegios sí se lo
hayan tomado así. Pero sabed que esto será a costa de recuperar los días perdidos en vacaciones.
Estamos en una situación de riesgo de contagio que invita a quedarse en casa (las canchas y parques
de Madrid ayer estaban abarrotados a más no poder): evitad por el bien de vuestros mayores las
reuniones multitudinarias. Una manera de hacerlo es hacer los trabajos diarios que iré poniendo.

## Metodología.

En este directorio de evaluación 3 iré poniendo lo que haya que hacer diariamente.

Todos los días debéis de subir una captura de pantalla del sistema donde se vea el
día y la hora que es. Así como el historial de la terminal desde la línea en la que
habéis empezado ese día a trabajar hasta la que habéis terminado.

Habéis de abriros una cuenta en gitlab y luego crear el repositorio os-subject.
En el habéis de hacer un directorio por día de clase y subir el trabajo solicitado
así como la captura y el historial.

Tres son los tesoros que os acompañan en este viaje. Estos son:
- El conocimiento. (Que intentaremos ir poniendo por aquí)
- Vuestros compañeros. El más importante. Os darán apoyo, compañía y os van a ser muy útiles a la hora de resolver dudas.
- El maestro. Si no conseguís, primero buscando internet información, ni, segundo, a través de un compañero resolver la duda,
podéis contactar conmigo en josemaria.gonzalez@salesianosdosa.com

Un saludo, y ánimo. ¡ Vamos a por todas !