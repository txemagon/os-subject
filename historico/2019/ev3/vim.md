vi

Modo comando y modo edición

`<rep>comando`

Entrar en modo inserción: 
i: Insertar a la izq del cursor.  
a: Insertar a la der del cursor.  
I: Insertar inicio de línea.  
A: Inserta fin de línea.  
o: Insertar en la línea de abajo.  
O: Insertar en la línea de arriba.  


Salir a modo comando: Esc

Movimientos

w: Avanzar de palabra en palabra.  
b: Ir hacia atrás.  
e: Ir al final de la palabra.  

gg: Ir a la primera línea.  
G: Ir a la última.  
3G: Ir a la línea 3.  

Otros
u: undo.  
ctl+R: Redo.  
x: Cortar un caracter.  
`d<movimiento>`: Borrar.  
p: Pegar a la derecha.  
P: Pegar a la izquierda.  

`:q!`: Salir.  

