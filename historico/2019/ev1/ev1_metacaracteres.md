# Metacaracteres

`*`: 0 o más caracteres cualesquiera.  
`?`: 1 caracter cualquiera.  
`[aou]`: Conjunto de selección.
`[!aou]`: Conjunto de selección inverso.
`[a-z]`: Rango  
`{}`: Sacar un factor / raíz / lexema común.  
`"`: Kriptonita para los espacios.  
`'`: Kriptonita para todo.  
`\`: Kriptonita para el siguiente caracter.  
`~`: Home.  
`~user`: Home de user.  
`.`: Directorio actual.  
`..`: Directorio de arriba.  
`$`: Dime cuánto vale.  
`()`: Subshell.  
`&`: Ejecuta en segundo plano.  
`space`: Parte palabras.  



