# Atajos

## Escritorio
`Ctl Alt Down`: Bajar al siguiente escritorio.  

## Terminal
`Ctl Alt t`: Abrir una terminal.  
`Ctl Shft t`: Nueva pestaña.  
`Alt <n>`: Activa la pestaña n.  
`Tab`: Autocompleta. Sugiere comandos que empiecen así.  
`Ctl c`: Interrumpe un programa.  
`Ctl l`: Borra la pantalla.  
`Ctl Shf c`: Copia.  
`Ctl Shf v`: Pega.  
`Ctl z`: Pasar a segundo plano (*background*).

## Ventanas
`Ctl W`: Cerramos la ventana, pero no el programa.  
`Ctl Q`: Cerramos el programa  (programas).  
`Alt F4`: Cerramos el programa (SO).  
`Ctl z`: Deshacer.  
`Ctl y`: Rehacer.  

## Explorador de Archivos (Nautilus)
`F2`: Renombrar.  
`Ctl-l`: Abrir la barra de direcciones _location_.  
