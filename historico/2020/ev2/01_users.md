# GESTIÓN DE USUARIOS

1. Conseguir Acceso remoto

.bashrc
non login shells

shell vs kernel

## Acceso Remoto

Necesito acceso de root  
Opciones: 
1. Conectarme con un usuario normal.  
2. He probado a ver si se la clave de root (`su`)  
3. Si no la sé puedo hacer (`sudo su`)  
4. Si no soy sudoer pero tengo acceso físico: Puedo arrancar la consola de root desde el menú de arranque.  

También necesito acceso de root ssh.

Opciones:
1. Habilitar el acceso con contraseña (mala idea):


### Habilitar la conexión de root con contraseña
1. Editar /etc/ssh/sshd_config
1. Cambiar la política por: `PermitRootLogin yes`
1. Reiniciar el servicio: `service sshd restart`

### Habilitar la conexión de root sin contraseña

1. Generar un par de claves: `ssh-keygen -t ed25519 -a 100 -f ~/.ssh/root_bussSrv.key -C root@ticbussiness.com`
1. Copiar la contraseña a la máquina remota:
    1. `ssh-copy-id -i ~/.ssh/root_bussSrv.key.pub root@172.19.70.1`
    1. `scp ~/.ssh/root_bussSrv.key.pub root@172.19.70.1:~/.ssh/authorized_keys`
    
### Añadir los usuarios



## Comandos

`su [<usuario>]`  
`passwd root`  
