# tar: Tape ARchive

## Opciones:

`x`: extraer  
`v`: verbose (contar todo lo que hace = prólijo = verbose)  
`z`: zip (comprimir)  
`c`: crear  
`f <nombre fichero>`  

## Copiar sin comprimir:
```bash
$ tar cf docs.tar Documentos
```

## Comprimiendo:
```bash
$ tar czf misdoc.tgz Documentos
$ tar cz Documentos > misdoc.tgz
```

Para pasar el archivo por un compresor  

```bash
$ tar c Documentos/ | gzip > docs.tar.gz
```

Para mandar en red:  
```bash
emisor$ cat docs.tar.gz | nc -q0 127.0.0.1 9999
recptr$ nc -l 9999 > los_docs_de_este.tar.gz
```

- Opción 1
```bash
recptr$ cat los_docs_de_este.tar.gz | gunzip > losdocos.tar
```
- Opción 2
```bash
recptr$ cat los_docs_de_este.tar.gz | gunzip | tar x
```

### Copiar directorios  
```bash
emisor$ tar cz Documentos/ | nc -q0 localhost 9999
recptr$ nc -l 9999 | tar xz
```

Para dar fiabilidad a la transmisión:
- Lo partimos en trozos
```bash
$ split -b1M misdoc.tgz tmp/jim_
```
- Para montarlo:
```bash
$ cd tmp
$ cat jim_* > misdoc.tgz

emisor$ tar -c /some/dir/to/copy | pv --size `du -sb /some/dir/to/copy | cut -f1` | pigz -5 | nc -l 9999
recptr$ nc source_server 9999 | gunzip | tar xvf -
```
