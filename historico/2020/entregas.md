# ENTREGAS BLOGGER

Cada entrega se redactará como un artículo o tutorial, procurando 
que aunque se mantenga la legibilidad, el texto sea corto y esté
trufado de imágenes o cortes de vídeo explicativos.

## Instalación

### Entrada 1
Instalar en Virtual Box un sistema dual con Windows y Ubuntu

## Gestión de Usuarios

### Instalación de acceso ssh mediante clave.

### Entrada 2
Generar un par de claves en la máquina nativa e instalar la clave pública en la máquina anfitrión. (Acordáos de configurar la red como Adaptador puente en la máquina virtual.)

### Entrada 3
Mostrar la gestión (creación, borrado, modificación y consulta) de usuarios y grupos.

### LDAP

- Crear un servidor de ldap y crear una conexión con _Apache Directory Studio_

- Crear una máquina virtual y configurarla como cliente para autentificarse contra el servidor.
