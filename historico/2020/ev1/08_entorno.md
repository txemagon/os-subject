# VARIABLES DE ENTORNO

`HOSTNAME`: Nombre del equipo.
`HOME`: Directorio Home del usuario.
`IFS`: Separador Interno de Campos.
`LANG`: Idioma de la terminal.
`PS1`: Command Prompt
```bash
PS1="\t$ "  # Pone la hora en el Prompt
```
`PATH`: Lista de directorios separada por `:`. Los ejecutables se buscan en los directorios del `PATH`.

`EDITOR`: Establece el editor por defecto.
