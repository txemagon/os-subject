# Sistemas de Ficheros

# Línea de comandos
`<nombre>`: Ejecuta un comando.  
`comando argumento1 argumento2 ...`  
El comando es el argumento 00  
Lo que empieza por guión es una opción  

# Gestión de Ficheros
Comandos para copiar, mover, etc.

`ls`: lista ficheros de un directorio.  
`ls -i`: inodos  
`ls -h`: Formato humano  
`ls -l`: Format largo  
`[dc-bspl]rwxrwxrwx`  
`         pppgggooo`  
`ln viejo nuevo`: Da un nombre extra en el directorio. Hardlink.  
`ln -s viejo nuevo`: Crea un enlace simbólico. Softlink. (acceso directo).  
`rm nombre`: Quita un nombre (borra) a un inodo.  
`rm -rf directorio`: Borra un directorio y todo lo que hay dentro.  
`-r`: Recursivo  
`-f`: Forzar  
`mv`: Mueve / Renombra  
`rename 's/busqueda/sustitucion/' ficheros'`: Renombra múltiples ficheros.  
`mkdir`: Hacer un directorio `-p`  
`rmdir`: Borrar un directorio vacío.  
`cd`: Cambiar un directorio.  
`pushd`: Cambia a un directorio guardando en el que estoy  
`popd`: Recupera un directorio guardado.  
`pwd`: Directorio actual de trabajo.  
`cp`: Copia un fichero o directorio.  

```bash
cp existente nuevo
cp -R directorio/* directdestino/
```

# Permisos de los Ficheros
`chmod 755 <fichero>`  
`chmod +x` Permiso de ejecución para todos  
`chmod -x` Fuera el permiso de ejecución para todos  
`chown <user>[:<group>]`: Cambiar el propietario / grupo de un fichero.  
`touch`: Si no existe el fichero lo crea. Si existe cambia la fecha de modificación.  

# Ficheros Especiales
`/dev/zero`: Siempre leemos un cero desde él.  
`/dev/null`: Echemos lo que echemos nunca contiene nada (papelera).  
`/dev/random`: Da números aleatorios. Necesita de eventos en el sistema para generarlos.  


