# Gestión de Discos

## Comandos para el administrador
`df`: *Disk Free*. La cantidad de espacio en los discos (`df -h`).   
`du`: *Disk Usage*. Uso de disco. `du -sh` (_s: squeeze_).  
`fdisk`: Modifica las particiones de un disco duro.  
`fdisk -l`: Listamos las particiones.  
`mount`: Monta/Cuelga el sistema de archivos de un disco duro en/de un directorio.  
`fsck`: Chequear la integridad de un disco duro.

![](img/mount-diag.jpg)

### Programas de Ventanas

1. Analizador de Discos
1. Gparted
1. Discos


## Comandos de usuario

`dd`: *Data Definition*. Copiar datos.  
> `if`: *Input File*. Fichero de entrada  
> `of`: *Output File*. Fichero de salida  
> `bs`: *Block Size*. Tamaño de cada lectura.  
> `count`: *count*. Número de lecturas.  
> EJEMPLO:  
>    `dd if=/dev/zero of=diskimage bs=1M count=1024`  

`mkfs.ext4`: *Make FileSystem*. Crea un sistema de ficheros.
> SINTAXIS: `mkfs.ext4 <fichero>`

## Cómo crear una imagen de disco

```bash
dd if=/dev/zero of=diskimage bs=1M count=1024  # Crea un fichero de 1Gb con 0's
mkfs.ext4 diskimage				# Crear un sistema de ficheros
sudo losetup --find --show ~/tmp/diskimage   	# Asocia un fichero con el primer
						# dispositivo de bucle libre.
mkdir copiando					# Crea un directorio par 
						# montar el dispositivo de bucle
sudo mount /dev/loop24 `pwd`/copiando		# Monta el dispositivo
						# en el directorio
df -h copiando/					# Ver la ocupación del "disco".
sudo umount /dev/loop24				# Desmontar la unidad.
rmdir copiando/					# Borrar el directorio
sudo losetup -d /dev/loop24			# Desasociar el dispositivo de bucle
						# del fichero.
```

## fstab

_File Systems Table_. Indica cómo montar los dispositivos.

```
<dispositivo> <punto_de_montaje> <sistema_de_archivos> <opciones> <dump-freq> <pass-num>
```
