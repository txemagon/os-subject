# Control de Accesos

## Listado de permisos

| Lectura | Escritura | Ejecución      |
|:-------:|:---------:|:--------------:|
| r       | w         | x _execution_  |
|         |           | s _setuid_     |
|         |           | t _sticky bit_ |


Cuando se asignan permisos recursivamente se puede poner la opción `X` mayúsucula. Eso significa que los directorios tendrán permiso de ejecución. No así los ficheros.

Para que un script sea ejecutable tiene que ser legible.

Los ficheros que están en un directorio que no tiene permiso de ejecución no se ejecutarán.

### Sticky Bit

En ficheros, el programa es mantenido en el swap y puede ser ejecutado por otros usuarios.
En directorios, sólo puede ser renombrado o borrado por el propietario, aunque otros tengan permiso de escritura.

### Setuid

Un usuario normal puede ejecutar un programa que necesita permisos de root.

```bash
# Añade permisos SUID a un binario de ejemplo (usuario)
chmod u+s /home/user/ejecutable

# Elimina permisos SUID a un binario (usuario)
chmod u-s /bin/ping

# Añade permisos GUID a un binario de ejemplo (grupo)
chmod g+s /home/user/ejecutable

# Elimina permisos GUID a un binario de ejemplo (grupo)
chmod g-s /home/user/ejecutable

# Visualización de los permisos de un binario con atributos SUID
ls -al /bin/ping

-rwsr-xr-x 1 root root 35712 2011-05-03 12:43 /bin/ping

```

## Propiedad

```bash
chown <user> pograma 
chown :<group> pograma 
chown <user>:<group> pograma 

# Ejemplos

# Numérico
chmod 755 filename    # rwxr-xr-x

# Con letras

# Absolutos
chmod u=rwx filename
chmod go=rx filename  # El grupo y los otros pueden leer y ejecutar

# Añadidos
chmod g+w foobar      # Permiso de escritura al grupo
chmod +w              # Permiso de escritura a todos
chmod a+w             # Equivalente al anterior xq a: all
chmod o-x             # Quitar permiso de ejecución a los otros

# Recursivamente
chmod -R a+rX ./data/ # Asignar recursivamente en todos los subdirectorios.


# Copiar permisos
chmod g=u foobar      # Copiar los permisos del grupo al usuario
chmod g=wu foobar     # No vale. No se puede añadir permisos (w) mientras se copia.

```

Para ver los permisos usamos el comando:

```bash
stat -c %a filename
```


## Propiedad por defecto

Con umask damos una información al directorio de cuales serán los permisos por
defecto que se asignará a cada fichero nuevo creado.

`umask` utiliza el complemento a 7 de los permisos.

| Octal | Binary | 	Meaning       |
|-------|:------:|--------------------|
| 0 	| 000 	 | no permissions     |
| 1 	| 001 	 | execute only       |
| 2 	| 010 	 | write only         |
| 3 	| 011 	 | write and execute  |
| 4 	| 100 	 | read only          |
| 5 	| 101 	 | read and execute   |
| 6 	| 110 	 | read and write     |
| 7 	| 111 	 | read, write and execute |

Para mostrar:

```bash
umask

0027      # Salida en octal

umask -S  # Salida simbólica

u=rwx,g=rx,o=

# Para que el propietario tenga todos los permisos sobre los ficheros nuevos y los demás no:
umask 077
```

## Permisos

### Individuales

```
# Permiso de ejecución
chmod +x pograma  # Dar a todos
chmod -x pograma  # Quitar a todos

chmod u+x pograma # Al propietario
chmod g+x pograma # Al grupo
chmod o+x pograma # Al resto

# Ejemplos
chmod +r *.txt
chmod -R +r pepe/*.txt # Aplicar recursivamente en todos los subdirectorios.

```


### Control de Acceso

Extraído de la wiki de Arch: https://wiki.archlinux.org/index.php/Access_Control_Lists_(Espa%C3%B1ol)

#### Comprobación Previa

Comprobamos que la partición está montada con la opción acl

```bash
tune2fs -l /dev/sdXY | grep "Default mount options:"
```

Debe poner:

```
Default mount options:    user_xattr acl
```

Para cambiar las opciones de montaje:

```bash
tune2fs -o acl /dev/sdXY
```

#### Implementación

Para agregar permiso a un usuario (user es el nombre del usuario o el ID): 

```bash
setfacl -m "u:user:permissions" <file/dir>
```

Agregar permisos para un grupo (group es el nombre del grupo o el ID del grupo):

```bash
setfacl -m "g:group:permissions" <file/dir>
```

Para permitir que todos los archivos o directorios hereden las entradas de ACL desde el directorio con: 

```bash
setfacl -dm "entry" <dir>
```

Para eliminar un entrada específica: 

```bash
setfacl -x "entry" <file/dir>
```

Para eliminar todas las entradas: 

```bash
setfacl -b <file/dir>
```

Para mostrar los permisos use: 

```bash
getfacl <nombre archivo>
```

#### Ejemplo


##### Ejemplo 1:

Establecer todos lo permisos para el usuario johnny en el archivo con nombre “abc”: 

```bash
setfacl -m "u:johnny:rwx" abc
```

Comprobar permisos:

```bash
getfacl abc
```

Salida

```
# file: abc
# owner: someone
# group: someone
user::rw-
user:johnny:rwx
group::r--
mask::rwx
other::r--
```
La salida del comando `ls` tiene un + en los permisos:

```bash
ls -l /dev/audio

crw-rw----+ 1 root audio 14, 4 nov.   9 12:49 /dev/audio
```

##### Ejemplo 2

```bash
setfacl -m "u:webserver:--x" /home/geoffrey  # Webserver puede ejecutar.
chmod o-rx /home/geoffrey                    # Al resto le quitamos permisos de lectura y ejecución.

```
