# Metacaracteres

`*`: 0 o más caracteres cualesquiera.  
`?`: 1 caracter cualquiera.  
`[aou]`: Conjunto de selección.  
`[!aou]`: Conjunto de selección inverso.  
`[a-z]`: Rango  
`{}`: Sacar un factor / raíz / lexema común.  
`"`: Kriptonita para los espacios.  
`'`: Kriptonita para todo.  
`\`: Kriptonita para el siguiente caracter.  
`~`: Home.  
`~user`: Home de user.  
`.`: Directorio actual.  
`..`: Directorio de arriba.  
`$`: Dime cuánto vale.  
`()`: Subshell.  
`&`: Ejecuta en segundo plano.  
`space`: Parte palabras.  
`$()`: Sinónimo del backtick. Ejecuta un comando.  
`!`: History expansion

```bash
# Backup Rotativo de una BBDD
mysqldump <base>  backup-dia_`date +%A`
```

`$(())`: Expansión aritmética.

```bash
echo $(( 2#101 + 7)) # => 5
echo $(( 3#201 + 7)) # => 26
echo $((  0x1A + 7)) # => 33
echo $((   017 + 7)) # => 22
```



