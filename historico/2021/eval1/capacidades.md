# Capacidades


## Gestión de ficheros
1. Crear un directorio. (`mkdir [-p]`)
1. Eliminar Ficheros (`rm`)
1. Eliminar un directorio.
    1. Vacío (`rmdir`)
    1. No vacío (`rm -r`)
    1. Con subdirectorios (`rm -r`)
1. Crear un fichero
    1. Vacío (`touch`)
    1. No vacío (redirección `<`)
    1. Oculto (empiezan por `.`)
1. Copiar
    1. Un fichero (`cp`)
    1. Un directorio (`cp -R`)
1. Listar un directorio (`ls`)
    1. Ver inodo (`ls -i`)
    1. Ver permisos (`ls -l`)
    1. Ver en formato humano (`ls -h`)
    1. Ver ficheros ocultos. (`ls -a`)
1. Renombrar un fichero (`mv`)
1. Cambiar un fichero de directorio (`mv`)
1. Cambiar los permisos (`chmod`)
1. Cambiar el usuario y el grupo (`chown`)

