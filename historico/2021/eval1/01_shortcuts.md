# Atajos

## Escritorio
`Meta`: Sacar el menú.  

`Meta ↑`: Maximizar.  
`Meta ↓`: Restablecer.  
`Meta ←`: Acoplar a la izquierda.  
`Meta →`: Acoplar a la derecha.  

`Ctl Alt ↓`: Bajar al siguiente escritorio.  
`Ctl Shift Alt ↓`: Bajar una ventana al siguiente escritorio.  

`ImprPant`: Captura de pantalla.  
`Shft-ImprPant`: Capturar un recuadro.  
`Alt-ImprPant`: Capturar la ventana actual.  


## Terminal
`Ctl-Alt-F1`: Display Manager.  
`Ctl-Alt-F<x>`: Terminales.  
`Ctl-Alt-F7`: Ventanas.  
`Ctl Alt t`: Abrir una terminal.  
`Ctl Shft t`: Nueva pestaña.  
`Alt <n>`: Activa la pestaña n.  
`Tab`: Autocompleta. Sugiere comandos que empiecen así.  
`Ctl c`: Interrumpe un programa.  
`Ctl d`: Fin de fichero.  
`Ctl l`: Borra la pantalla.  
`Ctl Shf c`: Copia.  
`Ctl Shf v`: Pega.  
`Ctl z`: Pasar a segundo plano (*background*).  

## Ventanas
`Ctl W`: Cerramos la ventana, pero no el programa.  
`Ctl Q`: Cerramos el programa  (programas).  
`Alt F4`: Cerramos el programa (SO).  
`Ctl z`: Deshacer.  
`Ctl y`: Rehacer.  

## Explorador de Archivos (Nautilus)
`F2`: Renombrar.  
`Ctrl-Shft-n`: Nuevo directorio.  
`Ctl-l`: Abrir la barra de direcciones _location_.  
`Ctl-h`: Muestra los archivos ocultos.  
`Ctl-t`: Nueva pestaña.  
`Supr`: Enviar a la papelera.  
`Shft-Supr`: Borrar.  

## Editores de texto

Movimientos:  

`Ctl-→`: Avanzar de palabra en palabra.  
`Ctl-←`: Retroceder de palabra en palabra.  
`Ctl-↓`: Bajar al final de la línea.  
`Ctl-↑`: Subir al final de la línea.  
`RePag`: Sube una pantalla.  
`AvPag`: Baja una pantalla.  
`Inicio`: Inicio de línea.  
`Fin`: Fin de línea.  

`Shft-<Movimiento>`: Subrayar.  

## Firefox

`F5`: Recargar.  
`Ctl-F5`: Recargar borrando el caché.  
`Ctl-r`: Recargar borrando el caché.  
`F6`: Ir a la barra de direcciones.  
`Ctl-h`: Historial.  
`F12`: Sacar la consola.  
`Ctl-TAB`: Cambiar entre pestañas.  
`Alt-<numero>`: Cambiar a la pestaña &lt;número&gt;.  
`Ctl-Tab`: Cambiar entre pestañas.  
`Ctl-n`: Nueva ventana.  
`Ctl-Shft-p`: Nueva ventana privada.  
`Ctl-F4`: Cerrar una pestaña.  
`Ctl-w`: Cerrar una pestaña.  
`Ctl-click`: Abrir en una pestaña nueva.  
`Wheel-click`: (link) Abrir en una pestaña nueva.  
`Wheel-click`: (pestaña) Cerrar pestaña.  
`Ctl-t`: Abrir una pestaña en blanco.  
`Ctl-Shft-t`: Abrir una pestaña recién cerrada.  
`Ctl-F4`: Cerrar una pestaña.  
