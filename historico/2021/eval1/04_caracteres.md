# Sistemas de codificación

## Secuencias de Escape

Caracteres que empiezan por barra.Ej: `\x0a`

```bash
user@host:~$ echo -e "Hola\x0AAdios"         # Salto de línea
user@host:~$ echo -e "Hola\nAdios"           # Salto de línea
user@host:~$ echo -e "Voy a sacar un 1\x30"  # Añade un 0
user@host:~$ echo -e "Voy a sacar un 1\060"  # Igual pero en octal.
```

## ASCII

128 Caracteres.  

### Caracteres de Control

```
0x00   Nulo
0x07   Campana           \a
0x08   Retroceder        \b
0x09   Tabulador         \t
0x0A   Salto de línea    \n
0x0D   Retorno de Carro  \r
0x1B   Escape            \e

0x1F
```

### Caracteres Representables

Los hitos más importantes:

```
0x20: Espacio
0x30: '0'
0x41: 'A'
0x61: 'a'
```

## UNICODE

Se meten con `Ctrl+Shift+U`

Ejemplos:

&1F924: 🤤
&1F946: 🥆
&1F406: 🐆
&272f : ✯
&1F60E: 😆
&1F6E9: 🛩

## Caracteres de un Archivo

```bash
echo "Hola" > fichero.txt # Crea fichero.txt con el texto Hola
od <fichero>      # Octal dump. Volcado de Octetos.
od -tc  bases.md  # Para verlo tipo carácter.
od -tx1 bases.md  # En hexadecimal de byte en byte.
od -tcx1 bases.md # Caracter y hexadecimal.
```
