# SCRIPTS

## Motivación

Se pueden poner los comandos de uso frecuente en un fichero para ejecutarlos conjuntamente.

Si no se dice nada, se entiende que son comandos del bash. Últimamente otros lenguajes interpretados se permiten. Para ello se puede indicar con el shebang qué intérprete es necesario. (`#!/bin/bash`)

Al programa de guión hay que darle permisos de ejecución. (`chmod +x calculadora`) y lo ejecutaríamos como (`./calculadora`)

## Entrada y salida de datos

### Salida

`echo`: Imprimir. (`-e`: secuencias de escape) (`-n`: no imprimir el salto de línea).  

```bash
cat <<FIN
Elige Operación:

     1.- Sumar
     2.- Restar
     3.- Multiplicar
     4.- Dividir

FIN
```

`printf`: Imprimir con formato.  

### Entrada

```bash
read -p "Opción: " variable
# -p: prompt. Imprime una pregunta.
# -d delim: Cambiar el delimitador.
# -n ncars: Limitar la cantidad de caracteres  que puede leer.
# -r: No permite que las barras invertidas escapen caracteres
# -s: Lee en secreto; sin eco.
# -u df: Lee desde el fichero
```

La opción `-s` lee en secreto.

## Condiciones

### Lógicas
`-o`: Or (O)  
`-a`: And (Y)

### Relacionales

### Numéricas

`-ge`: greater or equal than  
`-gt`: greater than  
`-le`: less or equal than  
`-lt`: less than  
`-e`: Equal  


Para más información ver la sección [operadores](./04_operadores.md).

## Esctructuras de Control

### Alternativas

#### if

```bash
if [ $opcion -eq 2 ]; then
    echo "Has elegido restar."
else
    echo "No has elegido restar."
fi
```

#### switch case



### Iterativas

#### for

```bash
for i in hola 1 2 3 adios; do
     echo $i
done

```

#### while

```bash
while [ $# -gt 0 ]; do
    echo $1
    shift
done
```

