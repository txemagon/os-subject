# Expansión Aritmética

## Operaciones

| Operador | Explicación |
|----------|-------------|
| id++ id-- |   post-incremento y post-decremento de variable | 
| ++id --id |   pre-incremento y pre-decremento de variable  | 
| - + | más y menos unario | 
| ! ~ | negación lógica y a nivel de bits |
| **  | exponenciación |
| * / % |  multiplicación, división, resto | 
| + -   |  adición, substracción | 
| << >> |  desplazamiento de bits a izquierda y derecha | 
| <= >= < > | comparación | 
| == != | igualdad y desigualdad | 
| & |  AND a nivel de bits
| ^ | OR exclusivo a nivel de bits| 
| `|` | OR  a nivel de bits |
| && | AND lógico| 
| `||` | OR lógico | 
| expr ? expr : expr | operador condicional |
| = *= /= %= += -= <<= >>= &= ^= |= | asignación | 
| expr1 , expr2 | coma | 


## Bases

`0...`: Octal  
`0x...`: Hexadecimal  
`0X...`: Hexadecimal  
`base#...`: Base entre 2 y 64  


