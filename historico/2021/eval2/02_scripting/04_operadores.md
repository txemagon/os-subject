# Operadores

## Condiciones

Tradicionalmente:

```bash
[ <expresión> ]
```

La extensión del BASH:

```bash
[[ <expresión>  ]]
```

## Relacionales

Los operadores relacionales comparan dos elementos y devuelven verdadero (0) o falso (1).

### Cadenas de Caracteres


| Operador | Descripción |
|----------|-------------|
| `string1 = string2`  | Igualdad |
| `string1 == string2` | Igualdad con BASH |
| `string1 != string2`  | Desigualdad |
| `string1 < string2`  | Orden alfabético menor |
| `string1 > string2`  | Orden alfabético mayor |
| `string =~ regex`  | Expresión regular |
| `-z string` | Comprobar si tiene longitud 0 |
| `-n string` | Comprobar si no tiene longitud 0 |

### Números


| Operador | Descripción |
|----------|-------------|
| `-lt`  | Menor que |
| `-gt`  | Mayor que |
| `-le`  | Menor o igual que |
| `-ge`  | Mayor o igual que |
| `-eq`  | Igual |
| `-ne`  | Distinto |


### Sistema de Ficheros

| Operador | Descripción |
|----------|-------------|
| `-e <nombre>`  | <nombre> existe. |
| `-f <nombre>` | <nombre> es un fichero |
| `-s <nombre>`  | <nombre> no tiene tamaño cero |
| `-d <nombre>`  | <nombre> es un directorio |
| `-r <nombre>`  | <nombre> tiene  permiso de lectura |
| `-w <nombre>`  | <nombre> tiene permiso de escritura |
| `-x <nombre>` | <nombre> tiene permiso de ejecución |


## Ejercicios

1. Crear el script `saluda <nombre>`. Imprimirá en pantalla `Hola <nombre>`.  Si el nombre `boss` en vez de decir `hola boss` el script debe decir `I adore you.`

1. Crea el script `ordenado <palabra1> <palabra2>` que diga si las dos palabras están ordenadas alfabéticamente.

1. Crear el script `existe` que pregunte con `read` por una entrada del sistema de ficheros y compruebe si existe o no.
