# VARIABLES DE ENTORNO

## Terminal

Desde la terminal se suele acceder a estas variables:

`HOSTNAME`: Nombre del equipo.  
`HOME`: Directorio Home del usuario.  
`LANG`: Idioma de la terminal.  
`LC_ALL`: Locale. Poner `LC_ALL=c` delante de un comando para ver su salida en inglés (y poder buscar errores en google).  

## Command Prompt

Para cambiar el prompt:

`PS1`: Primary prompt string. Cadena primaria del prompt.  
`PS2`: Secondary prompt string. Cadena secundaria del prompt.  
```bash
PS1="\t$ "  # Pone la hora en el Prompt
```

Para más información acerca del prompt ver la [sección prompt](./02.3_prompt.md)

###
`PATH`: Lista de directorios separada por `:`. Los ejecutables se buscan en los directorios del `PATH`.

`EDITOR`: Establece el editor por defecto.

`$_`: Último parámetro al arrancar la shell.  
`$-`: Lista de opciones de la shell actual.  
`$$`: PID del proceso actual.  
`$IFS`: Separador utilizado para delimitar campos.  
`$!`: Último comando ejecutado en segundo plano.  
`$?`: Valor de retorno del último comando.  

