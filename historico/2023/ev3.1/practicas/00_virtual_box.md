# Virtualización

## Tipos de Sistemas de Virtualización

### Barebone

En vez de instalar un sistema operativo, se instala un virtualizador que sirve los sistemas operativos.

Por ejemplo:

1. Xen
1. Citrix Xen Server


### Sobre HOST

1. VMWare
1. VirtualBox
1. Virtuozzo


## Sistemas Operativos

¿ Qué sistemas operativos podemos instalar ?

### Windows

Se apoyan sobre DOS (_Disk Operative System_)

Evolución de Windows

```mermaid
graph TD

1.0 --> 2.0
2.0 --> 3.0
3.0 --> 3.1
3.1 --> 95
95 --> 98
98 --> Millennium
Millennium --> XP

NT --> NT3.1
NT3.1 --> NT3.5
NT3.5 --> NT4.0
NT4.0 --> 2000
2000 --> XP

XP --> Vista
Vista --> W7
W7 --> W8
W8 --> W10


95 -->|UI shared code| NT4.0

```

### UNIX

Es un sistema operativo creado por los laboratorios Bell AT&T. Del derivan o se inspiran:

1. Linux
1. Mac


### El mundo Linux

Algunas distros de Linux:

1. SolusOs
1. Arch
1. Debian
1. Suse
1. Mandrake
1. Red Hat

Algunos sabores de Debian


Algunos sabores:


### Sistemas Empotrados

1. ANDROID
1. Cámara Web
1. Kindle
1. Impresora


### Sistemas de Red

1. Chrome OS

# Instalación de una Máquina Virutal

## Configuración de VirtualBox

1. Habilitar la virtualización por Hardware. (BIOS)
