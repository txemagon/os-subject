# Información sobre Sucesos

## Arrancando GParted

Al arrancar el GParted no salía la ventana, para ello hemos procedido de la siguiente manera:

1. Abrir una terminal `Ctl Alt T`
1. Arrancar GParted desde la terminal para poder ver los errores: `sudo gparted`
1. Hemos comprobado que decía que no existe `display :0`


En ese momento hemos caído en la cuenta que el usuario había arrancado sesión en `Wayland`.


### Explicación

Linux habitualmente trabaja con un servidor de ventanas: `X`, usualmente referido como  `servidor de X`
o simplemente, `las Xs`.  Esta arquitectura permite ejecutar aplicaciones gráficas remotas mediante el comando
`ssh -X`. Se ejecutaría, así un programa en la máquina remota pero las ventanas, botones, etc (`toolkit`) se
proveería desde la máquina local.

En un intento de portar linux hacia el mundo móvil se ha creado `Wayland`, que es un sistema gráfico alternativo
a las `X`.

Por ende, todos los programas que empiecen por G (que viene de `Gnome`, un escritorio basado en `X`) no funcionan
si iniciamos sesión en `Wayland`.
