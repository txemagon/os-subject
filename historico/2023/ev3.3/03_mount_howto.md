## Crear una imagen iso
mkisofs -J -l -R -V "ikerCD" -o iker.iso iker_dir 

## Montar
sudo mkdir /media/txema/mi_cd
sudo mount -o loop iker.iso /media/txema/mi_cd

## Desmontar
sudo umount /media/txema/mi_cd
sudo rmdir /media/txema/mi_cd
