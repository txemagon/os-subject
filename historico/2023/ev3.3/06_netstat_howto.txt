Sockets
- unix
- inet
  - tcp: orientados a  conexión
  - udp: datagramas
arquitectura cliente servidor

¿Dónde está levantado el servicio?
Binds:
127.0.0.1 / localhost	- Sólo local
172.19.70.2 		- Sólo externo
0.0.0.0			- Cualquier sitio


netstat (la navaja suiza)

--interface 	= ifconfig
--route -r   	Enrutamiento
--program -p  	Programas que usan la red
              	[fuser -n tcp 80]
--listening -l	Conexiones que escuchan
-all -a		Todas las conexiones
-tcp -t		tcp
--numeric -n    Numerical addresses

netstat -tan

¿Qué hay en un puerto?
cat /etc/services | grep <puerto>

