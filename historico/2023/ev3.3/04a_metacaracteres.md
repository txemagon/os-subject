# Metacaracteres:

##	Filename expansion

`*`: Cualquier secuencia de caracteres. (cero o mas)  
`?`: Un único carácter cualquiera.  
`[]`: Un único carácter de los presentes. Clase de caracteres. Rango. Conjunto de selección, character set.  

>	`!`: Conjunto inverso.  
>	`^`: Conjunto inverso.  
>	`-`: Rango a-z => De la 'a' a la 'z' minúsculas.  

`{}`: Especifica raíces o átomos para el patrón.  

##	Otros
`\`: Criptonita para el siguiente.  
`"`: Criptonita para todo menos para el dólar y la barra `\`.  
`'`: Criptonita para todo.  
`sp`: Partir en palabras.  
`$`: Dime el valor de la variable.  
`$()`: Ejecuta y pon el resultado.  
`` ` ``: Ejecuta y pon el resultado.  
`$(())`: Expansión aritmética.  

/* Extendidos */  
`()`: A(boo)+z  
`^`: comienzo de línea  
`.`: In carácter cualquiera.  

Ejercicios:
Lista todos los ficheros que:
1. Contengan en alguna parte la secuencia 'astur'
1. Que sean equipo-<letra>.txt
1. Que sean, equipo-<vocal>.txt
1. Que sean cabrino-algo.algo ó porcino-algo.algo

Haz un directorio (mkdir) que 
1. Contenga espacios en el nombre.
1. Contenga un espacio al final.
1. Contenga una barra (\) en el nombre.

1. Escribe con echo 2 + 3 = `<resultado>`

