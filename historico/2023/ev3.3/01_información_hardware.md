# Información del Hardware

## Notación

### Discos

1. `fd<num>`: *fd0, fd1, ...*: Unidades de disquete.
1. `hd<letra>`: *hda, hdb, ...*: Discos duros antiguos - IDE.
1. `hd<letra><num>`: *hda0, hda1, hdb0, hdb1,...*: Particiones de discos.
1. `sd<letra>`: *sda, sdb, ...*: Discos serie - SATA
1. `sd<letra><num>`: *sda0, sda1, ...*: Particiones.
1. `scd<num>`: *scd0, scd1, ...*: Unidades CD-ROM SCSI
1. `sg<letra>`: *sga, sgb, ...*: Dispositivos genéricos (escáneres, etc).
1. `sg<num>`: *sg0, sg1, ...*: Disp. genéricos sistemas nuevos.

### Sistema de Ficheros

1. `auto`: SO apáñatelas tú.
1. `iso9660`: CD y DVD
1. `ext2`: Sistema
1. `ext3`: Ficheros
1. `ext4`: GNU/Linux
1. `reiserfs`: Linux
1. `msdos`: FAT12 o FAT16 (Archivos menores de 2GB)
1. `vfat`: FAT32 (Archivos menores de 4GB).
1. `ntfs`: NTFS Sistema de diario.
1. `smbfs`: Samba - Intercambio de ficheros.
1. `nfs`: Network File System. 
1. `hfs` y `hfsplus`: Apple Macintosh.

### Tipos de dispositivos

Estos dispositivos se leen:

1. caracter - character. 1 byte.
1. bloque - block. 4kb
1. bucle - loop: Permite leer de un fichero como si fuera un dispositivo de bloque.

## Comandos

**CPU:Central Processing Unit**  
`lscpu`: Información de las unidades de proceso.  
**RAM:Random Access Memory**  
`free -mh`: Memoria libre / Memoria total.  
**Información General**  
`lshw`: Información general de todo el hardware (`lshw -short`).  
`hwinfo`: Info general `hwinfo` > `lshw`. `hwinfo  --short`  

`dmidecode`: Analiza el hardware.  
- `dmidecode -t processor`: Info del procesador.
- `dmidecode -t memory`: Info de la memoria / ram.
- `dmidecode -t bios`: Detalles de la BIOS.
**Información por tipo**  
`lspci`: Listar PCI's.  
`lsscsi`: Listar los dispositivos scssi/sata
`lsusb`: Listar los USB.  
**Informes**  
`lnxi`: Super informe de lo que hay en la máquina.  
`lsblk`: *List Bulk*. Lista de dispositivos de bloque (almacenamiento masivo).  









