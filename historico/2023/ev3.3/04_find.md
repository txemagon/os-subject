# BÚSQUEDAS

## Sintaxis

```bash
find <directorio de busqueda> [opciones]
```

`-name`:  Buscar usando los nombres de fichero. (Se pueden usar metacaracteres)  
`-iname`: Ignora el caso.  
`-type <f|d>`: Encontrar ficheros o directorios.  
`-user`: Encontrar por el propietario.  
`-size`: c (bytes), b (block - 512c), k(kilo), M(Megas) - (<), +(>)  


## Funciones Forenses

### (dias)
`-mtime`: Modificado  
`-atime`: Accedido  
`-ctime`: Cambiados los permisos  

### (Minutos)
`-mmin`  
`-amin`  
`-cmin`  

-o -or  
-exec <comando> {} \;  


Ejemplos:

- Todos los ficheros que empiezan por lib.  
    `find / -name "lib*"`

- Todos los directorios dentro de la carpeta HOME que contengan la letra 'e'.  
   `find $HOME -type d -name *e*`
   
- Buscar en el directorio actual.  
   `find . -type d -name "*e*"`
   `find `pwd` -type d -name "*e*"`
- Buscar todas las fotos menores de 2 megas de daniel.  
  `find . -name "*.jpg" -user daniel -size -2M`

- Todos los ficheros que han sido leídos en el último minuto.  
  `find / -amin 1`
  
- Todos los cambios de configuracion del último día.  
  `find /etc -ctime 1`
  
- Todos los documentos de word que han sido editados en los dos últimos días.  
  `find ~daniel -name "*.doc" -mtime 2s`
  
