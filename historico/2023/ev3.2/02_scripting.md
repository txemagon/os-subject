# SCRIPTING

## Notas preliminares

Empleo del $:

`$VAR`: Dime lo que vale VAR  
`${VAR}`: Dime lo que vale VAR  
`$(seq 1 20)`: Ejecuta seq y pon aquí el resultado. Equivalente al backtick.  
`$(( 2 + 3))`: Expansión aritmética. Opera y pon aquí el resultado.  

## Consideraciones Generales

1. Es vital comentar
1. `#!` sel llama shebang e indica con qué interprete se ejecutará el código.

## Variables y Parámetros

De la obligatoriedad de las llaves:

```bash
#!/usr/bin/bash
# ProgramaTxote v1.0

USER=Juanma

echo "Txematxote"
echo $USERtxote    # No existe la variable USERtxote.
echo $USER txote   # Espacio en blanco no deseado.
echo ${USER}txote  # Son imprescindibles las llaves.
```

De los Parámetros:

```bash
#!/usr/bin/bash
# programaTxote v1.0
# 
# ./programaTxote Juanma


echo ${1}txote  # Son imprescindibles las llaves.
```

## Parámetros


