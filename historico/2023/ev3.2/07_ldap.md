# LDAP

    slapd – demonio LDAP autónomo.
    slurpd – demonio de replicación de actualizaciones LDAP autónomo.
    Rutinas de biblioteca de soporte del protocolo LDAP
    Utilidades, herramientas y clientes.


## Claves de ldap
Nombre Distinguido o «Distinguished Name (DN)

cn o Common Name
mail para las direcciones de correo electrónico
jpgePhoto puede contener una foto en el formato binario JPEG


LDAP permite el control de cuáles atributos necesitamos para una entrada mediante el uso de un atributo especial denominado objectClass.

objectClass determina las Reglas del Esquema o Schema Rules que la entrada debe obedecer.

 RDN cn=Frodo
 DN completo es cn=Frodo Bagins,ou=Anillos,o=Amigos,st=Habana,c=cu
 
 Entry (objeto): Cada unidad de ldap
 dn: distinguished name
 o: organization
 ou: organizational unit
 dc: domain component (componente de dominio)
 cn: common name
 
 
## Información

    Autenticación de Máquinas
    Autenticación de Usuarios
    Usuarios y Grupos del Sistema
    Libreta de Direcciones
    Representaciones Organizativas
    Seguimiento de Recursos
    Almacén de Información Telefónica
    Administración de Recursos de Usuarios
    Búsqueda de Direcciones de Correo
    Almacén de Configuraciones de Aplicaciones
    Almacén de Configuraciones de plantas telefónicas PBX
    etcétera…

## Programas Aplicables

- Servicio de Directorio o Directory Service basado en OpenLDAP
- Servicios NTP, DNS y DHCP independientes
- Integrar Samba al LDAP
- Posiblemente desarrollaremos la integración de LDAP y Kerberos
- Administrar el Directorio con la aplicación web Ldap Account Manager.


## Instalación

```bash
sudo apt-get install slapd
sudo apt-get install ldap-utils
```

Para habilitar el servicio:

```bash
systemctl enable slapd
```

## Configuración

Establecer la contraseña de administrador:

```bash
ldappasswd
```

Los ficheros de configuración están en:

```
/etc/openldap/slapd.d
```

y se pueden editar directamente, pero es mejor usar

```bash
ldapmodify
```

### Modificación

OpenLDAP almacena tu información en los archivos bdb o hdb.

```bash
/etc/openldap/slapd.d/cn=config/olcDatabase={2}hdb.ldif
```

