# Contenedores

## Introducción

Hola a todos. Hasta hoy hemos visto instalaciones nativas e instalación de máquinas virtuales.

Sin embargo, en los últimos años han tomado muchísima fuerza los contenedores.

Cuando instalamos sistemas operativos, desde hace unos años, es posible eludir la instalación del kernel. Es decir, la parte del sistema operativa que gestiona la memoria y otros componentes de la máquina física.

Los contenedores han resultado ser un inventazo en las siguientes áreas de aplicación.

1. Máquinas Virtuales. Son mucho más rápidas las máquinas virtuales cuando no tienen que emular el hardware y usan el kernel nativo. Un ejemplo: el programa Cajas (Boxes).

1. Distribución. ¡Qué lío las dependencias! Cuando instalamos software nuevo en el equipo teníamos que asegurarnos que todo el software que necesitaba estaba instalado en la versión correcta.

¡Pero eso era antes!

Ahora podemos instalar una distribución entera que contenga sólo los componentes requeridos. Eso es, por ejemplo: ¡ SNAP !

1. Sistemas empotrados. Imaginaros que tenemos un microprocesador de 30€, como pueda ser la Raspberry Pi y queremos que realice una tarea de control específica. Pues lo mejor es instalar esa aplicación ad hoc
desde un repositorio central de contenedores como puede ser Docker.


## Jaulas

### Un poco de historia

Antes de los contenedores enjaulabamos (y seguimos enjaulando) los programas. También a eso le llamamos rootear (rutear) un programa. La idea es aislarlo del resto del sistema operativo de manera que los 
usuarios de ese programa no puedan afectar y/o poner en peligro el resto del sistema. 

La idea fundamental que da origen a las jaulas consiste en hacerles creer que el directorio en el que se hace la instalación es el directorio root (`/`). Para que el programa pueda acceder a las librerías necesarias
también tendremos que instalar en el directorio de instalación.

### Anatomía del Comando

```
chroot [-OPCION] <NUEVOROOT> [COMANDO [ARGUMENTOS]* ]

DESCRIPCION
       Ejecutar COMMANDO con el directorio root puesto en NUEVOROOT.

       --groups=G_LIST
              Añadir grupos suplementarios g1,g2,..,gN

       --userspec=USER:GROUP
              Usuario y grupo con el que queremos iniciar la jaula.

       --skip-chdir
              no camibiar al directorio '/'

       --help muestra la ayuda

       --version
              mostrar la versión de chroot

       Si no se proporciona un comando, se ejecuta una shell, concretamente
       la que haya en la variable de entorno SHELL '"$SHELL" -i' (default: '/bin/sh -i').
```

### Un ejemplo

Vamos a ejecutar el comando `bash` (que es la terminal) pero rooteado. Esa terminal va a poder ejecutar un número reducido de comandos.
Concretamente: 

```bash
touch
ls
rm
```

Para ello vamos a seguir los siguientes pasos.

1. Crear el directorio jaula
1. Añadir los subdirectorios necesarios para las librerías.
1. Copiar los comandos que vamos a enjaular.
1. Averiguar las librerías requeridas (dependencias) por los comandos.
1. Cambiar al directorio jaula (rootear el bash).


#### Crear el Directorio Jaula

```bash
mkdir jaula
```

#### Añadir los Subdirectorios Necesarios para las Librerías

```bash
mkdir -p jaula/{bin,lib,lib64}
```

#### Copiar los Comandos que Vamos a Enjaular

```bash
cp -v /bin/{bash,touch,ls,rm} jaula/bin
```

La opción `-v` (verbose) nos da información de lo que va haciendo el comando.

En este caso en concreto:

```
'/bin/bash' -> 'jaula/bin/bash'
'/bin/touch' -> 'jaula/bin/touch'
'/bin/ls' -> 'jaula/bin/ls'
'/bin/rm' -> 'jaula/bin/rm'
```

#### Averiguar las Dependencias de Cada Comando

Para resolver las dependencias contamos con el commando `ldd`.

Ahora tenemos que indagar para cada uno de los cuatro comando anteriores de qué librerías dependen.

```bash
ldd /bin/bash
```

Arroja la siguiente salida:

```
	linux-vdso.so.1 (0x00007fff1f9b1000)
	libreadline.so.8 => /usr/lib/libreadline.so.8 (0x00007fa940769000)
	libdl.so.2 => /usr/lib/libdl.so.2 (0x00007fa940763000)
	libc.so.6 => /usr/lib/libc.so.6 (0x00007fa94059a000)
	libncursesw.so.6 => /usr/lib/libncursesw.so.6 (0x00007fa940529000)
	/lib64/ld-linux-x86-64.so.2 => /usr/lib64/ld-linux-x86-64.so.2 (0x00007fa9408f3000)
```

Es muy probable que en tu equipo no aparezca el directorio `/usr`. No te preocupes, óbvialo.
Y empezamos el proceso de copia:

```bash
 cp -v /usr/lib/libreadline.so.8 jaula/lib
```

Sigue con cada una de ellas, y fíjate que la última no está en el directorio `lib`, sino en el `lib64`,
de manera que el directorio de destino será `jaula/lib64`.

Si alguno de los restantes comandos (`rm`, `ls` o `touch`) añadiese alguna dependencia no copiada previamente,
también deberás copiarla.

En mi caso, al ejecutar:

```bash
ldd /lib/libncursesw
```

#### Cambiar al Directorio Jaula

```bash
sudo chroot jaula /bin/bash
```

Esto quiere decir se inicie la jaula en el directorio `jaula` considerando a éste como el raíz (/).
Luego le pedimos que arranque el comando `/bin/bash` (dentro del árbol de `jaula`).


Para salir de la jaula escribe

```bash
exit
```

### Algunos Errores Típicos

Si se te ha olvidado copiar alguna dependencia es posible que no te deje arrancar la jaula.

Una solución rápida, aunque menos segura, es hacer un enlace simbólico (acceso directo dirían
nuestros amigos de otros sistemas operativos) a los directorios originales.

Borra `jaula/lib` y `jaula/lib64` y prrueba a hacer:

```bash
sudo ln -s /usr/lib/ lib
sudo ln -s /usr/lib64/ lib64
```

E intenta arrancar la jaula de nuevo.
