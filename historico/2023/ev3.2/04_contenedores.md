# Contenedores

Los contenedores son una versión mejorada de las jaulas.

## Systemd-nspawn

Los comandos que hemos visto en clase:

```bash
# Descargar un sistema operativo
debootstrap xenial miubuntu http://archive.ubuntu.com/ubuntu

# Iniciarlo sin el proceso de login
systemd-nspawn -D miubuntu

# Crear un usuario nuevo que pueda hacer sudo
adduser admin
usermod -aG sudo admin

# Arrancar el contenedor
systemd-nspawn -b -D miubuntu

# Desde otra pestaña podemos
machinectl list
machinectl status miubuntu
machinectl poweroff miubuntu
```

## Docker

Docker es un gestor de contenedores

```bash

# Instalamos docker
sudo apt install docker.io

# Descargar una imagen de contenedor
docker pull sonarqube	

# Ver las imágenes que tenemos
docker images

docker run -d --name sonarqube -p 9000:9000 sonarqube
# -d: detach            Arrancar el programa desvinculándolo de la terminal.
# -p: mapeo de puertos  Mapear el puerto 9000 local al 9000 del contenedor.

docker ps       # Comprueba los contenedores que están levantados.
docker ps -a    # Comprueba los contenedores que están levantados. 	
docker stop sonarqube
docker rm sonarqube

# Desinstalamos docker porque secuestra la conexión de red
sudo apt purge docker.io
```
