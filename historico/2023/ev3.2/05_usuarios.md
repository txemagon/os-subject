# Gestión de Usuarios y Grupos

No todos los usuarios son personas.  Ejemplo:

- Apache => www-data

Las acciones (CRUD) son:

- listar.
- ver.
- crear.
- modificar.
- borrar.

Hay usuarios locales y usuarios en red.
LDAP (Light Directory Access Protocol)
Apache Directory Studio (IDE que permite gestionar el directorio activo)

## Ficheros Importantes

### Formato del Fichero /etc/passwd

Campos:

- nombre de login
- password encriptado opcional
- ID del user
- ID del grupo
- Nombre del Usuario o Comentario
- Directory HOME
- Intérprete de comandos

### Fichero /etc/shadow

Campos:

- nombre del login
- contraseña encriptada
- día del último cambio de contraseña
- minímos días con esta contraseña
- máxima edad de la contraseña
- periodo de aviso de cambio de contraseña
- periodo de inactividad de la contraseña
- fecha de expiración de la contraseña
- reservado

### /etc/sudoers

```bash
update-alternatives --config editor
visudo
```

```
user ALL=(ALL:ALL) ALL

Primer ALL => Todos los host que utilicen este /etc/sudoers
Segund ALL => Permitir ejecutar todos los programas como cualquier usuario.
Tercer ALL => Permitir ejecutar todos los programas como cualquier grupo.
Cuarot ALL => Puede ejecutar todos los comandos.
```


### Todos los Usuarios que Hay en el Sistema

```bash
ls /home                      # Usuarios que tienen directorio HOME.
cat /etc/passwd               # Todos los Usuarios Locales.
cat /etc/passwd | cut -f1 -d: # Mostrar sólo los usuarios.
cat /etc/passwd | grep <user> # Mostrar las líneas que contienen <user>
cat /etc/passwd | grep bash   # Usuarios con terminal
cat /etc/passwd | grep bash | cut -d: -f1

getent passwd                    # Usuarios Locales y en Red.
getent passwd | cut -f1 -d: | nl # Numerar usuarios totales.
```

El comando anterior no mostraría todos los usuarios porque:

1. No todos los usuarios tienen directorio HOME
1. No todos los directorios HOME están en `/home`

### Los Usuarios que se Han Conectado

```bash
last
cat /var/log/auth.log
```

### Los Usuarios que Están Conectados Ahora

```bash
who
users
```

### El Usuario que Soy Yo

```bash
echo $USER
whoami
```

### Investigar sobre un usuario

```
id         # Ver información del usuario conectado
id <user>  # Ver la información de user.
```

### Para cambiar de Usuario

```bash
su <user>       # Si sabemos el nombre de usuario
sudo su <user>  # Si somos sudoer.
```

## Creación de Usuarios

```bash
useradd
adduser   # Más completo
```

Opciones de `useradd`:

| Opción | Descripción                          |
|:------:|--------------------------------------|
| m      | Crear el directorio home en `/home`. |
| d      | Especificar un directorio home.      |
| M      | No crear el directorio home.         |
| g      | Especificar el grupo inicial.        |
| G      | Lista separada por comas sin espacios con los grupos extra del usuario. |
| s      | Especificar la shell                 |
| e      | Fecha de expiración. YYY-MM-DD       |
| f      | Num de días para que se deshabilite la cuenta después de haber exprirado. |
| u      | Asignar un uid                       |



## Modificación de Usuario

```bash
usermod -aG sudo <user>
chage
```

Opciones `usermod`:

| Opción | Descripción |
|:------:|-------------|
| a      | Añadir información. |
| b      | Permitir sin comprobar (badnames). |
| c      | Escribir el campo comentario en `/etc/passwd`. |
| d      | Especificar el directorio HOME. |
| e      | Fecha de expiración. |
| f      | Periodo de inactividad. |
| g      | Grupo inicial. |
| G      | Grupos extra. |
| l      | Nombre del login. |
| L      | Bloquear un Usuario. |
| m      | Mover el contenido de un usuario a un nuevo HOME. Hay que usar a la vez la opción `d`. |
| p      | Nueva contraseña encriptada. |
| s      | Especificar la shell. |
| u      | Especificar uid. |
| U      | Desbloquear el usuario. |

Opciones `chage`:

| Opción | Descripción |
|:------:|-------------|
| d      | Cambiar el número de días del último login |
| E      | Fecha de expiración |
| l      | Listar información sobre la edad de la cuenta |
| m      | Mínimos días para cambiar una contraseña |
| M      | Maxímos días hasta que expire la contraseña |
| W      | Perido de aviso - Warning - |

## Eliminación de Usuarios

```bash
deluser   # Borra un usuario
userdel   # Borra un usuario y sus ficheros
```



# GRUPOS


## Ficheros importantes

### /etc/group

Ejemplo:

```
adm:x:4:syslog,profesor
```

### /etc/gshadow

Campos:

- Nombre del grupo
- Contraseña del grupo
- Administradores
- Miembros


## Listar los Grupos

```
groups       # Locales
getent group # Todos los usuarios, incluidos los de red

```

## Añadir Grupos

```bash
# Añadir un grupo
groupadd -g <id_grupo> <nombre_del_grupo>
# También
addgroup <id_grupo>
# Añadir grupos a un usuario
usermod -G groupa,groupb <user-name>
```

## Modificar grupos

```bash
gpasswd
```

Añade o borra usuarios a grupos. Crea administradores y miembros de grupos.


