# Capacidades

Debes de saber hacer:

## Evaluación 1

El alumno debe ser competente en:

### Teoría

#### Atajos 

Manejar cualquier programa que no sea de dibujo sin el ratón:

1. Abrir la terminal.
1. Abrir cualquier programa.
1. Abrir nuevas pestañas y cambiar entre ellas.
1. Guardar, copiar, rehacer y deshacer.
1. Cerrar programas y ventanas.
1. Cambiar entre programas abiertos.
1. Cambiar entre ventanas del mismo programa.
1. Cambiar de nombre un fichero o directorio.
1. Poder ver los archivos ocultos.
1. Terminar un programa de la terminal (`Ctl-c`).


#### Bases

1. Convertir entre las distintas bases numéricas usando la base 10.
1. Hacer conversiones rápidas entre hexadecimal, octal y binario.
1. Interpretar el código ascii y unicode.
1. Ver el contenido de un fichero.


#### Ficheros

1. Listar los ficheros.
1. Ver los permisos
1. Cambiar los permisos
1. Mostrar la información en formato humano.
1. Ver el número de inodo.
1. Hacer enlaces duros (hardlink).
1. Hacer enlaces suaves (softlink - accessos directos).
1. Saber qué es una ruta absoluta y una ruta relativa.
1. Comprobar el directorio actual de trabajo.
1. Cambiar el nombre de un fichero.
1. Cambiar un fichero de directorio.
1. Copiar un fichero.
1. Copiar un directorio con todos los ficheros que tiene dentro y todos los subdirectorios que tiene.
1. Borrar ficheros.
1. Borrar directorios vacíos.
1. Borrar un directorio con todos sus subdirectorios.
1. Cambiar de directorio apilando o sin apilar el directorio actual.
1. Cambiar el propietario de un fichero.
1. Conocer los ficheros especiales.

#### Entrada / salida

1. Conocer el nombre y número de los streams.
1. Redirección simple y doble de salida.
1. Redirecciones simples de entrada.
1. Redirección Here Document.
1. Redirección Here String.
1. Redirección con tuberías.
1. Redirección de copia.

#### Programas de edición de texto

1. Mover entre palabras.
1. Subrayar.
1. Manejar el nano.

#### Metacaracteres

1. Seleccionar cualquier conjunto de ficheros usando filtros.
1. Introducir caracteres del código ascii usando la secuencia de escape.
1. Saber escapar los metacaractes para quitarles su función.
1. Usar las comillas dobles y las simples.
1. Ver el valor de una variable.
1. Ejecutar un comando y quedarnos con la salida (backup rotativo).
1. Ejecutar un programa en segundo plano.
1. Pasar programas activos a segundo plano.
1. Ver las tareas detenidas.
1. Reactivar tareas detenidas a primer plano.

### Práctica

1. Elegir un sistema operativo apropiado para cada tarea:
    1. UNIX, Linux, Windows, Mac. Tipos de usuarios y tareas.
    1. Genealogía de los Sistemas Operativos Linux.
     

1. Configurar un virtualBox:
    1. Memoria
    1. Tarjeta Gráfica: memoria, aceleración.
    1. Gestión de discos: dinámicos, bahías y puertos. Inserción de unidades.
    1. VirtualBox Extension y Guest Additions
    1. Gestión de red: NAT / Adaptador Puente

1. Habilitar la virtualización por hardware en la BIOS.
1. Conocer la nomenclatura de los discos duros serie, paralelo y sólidos.
1. Conocer los tipos de sistemas de ficheros: extN, FAT32, NTFS, HFS. APFS, SWAP
1. Manejar los puntos de montaje.
1. Conocer el software básico a instalar en cada equipo.
1. Gestión de discos con GParted.
1. Actualizar el menú de arranque (GRUB) con update-grub.
1. Configurar la red de un Ubuntu.

